﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using UnityEditor;
using UnityEngine.UI;
using System.IO;

// [ExecuteInEditMode]
public class TextManager : MonoBehaviour
{

    public static TextManager instance;
    public List<GameObject> icons = new List<GameObject>();

    public TMPro.TMP_FontAsset fontasset;

    // Start is called before the first frame update
    List<Text> parentText = new List<Text>();
    public StringData stringData = new StringData();

    [System.Serializable]
    public class StringData
    {
        public List<TutorialDatas> tutorialDatas = new List<TutorialDatas>();
        public List<TextDatas> textDatas = new List<TextDatas>();
        public List<NodeDatas> nodeDatas = new List<NodeDatas>();
    }

    [System.Serializable]
    public class TutorialDatas
    {
        public List<TutsDatas> tutsDatas = new List<TutsDatas>();

    }

    [System.Serializable]
    public class TutsDatas
    {
        public string tag = "";

        public int index;
        public int id;
        public int actionIndex;
        public List<TextData> textDataList = new List<TextData>();

    }


    [System.Serializable]
    public class NodeDatas
    {
        public int index;
        public List<TextData> nodeDataList = new List<TextData>();

    }

    [System.Serializable]
    public class TextDatas
    {
        // public bool isused = false;

        public string tag = "";
        public int id;
        public int index = -1;
        public int actionIndex = -1;
        public List<TextData> textDataList = new List<TextData>();

        public TextDatas(string tag, int id, List<TextData> textDataList)
        {
            this.tag = tag;
            this.id = id;
            this.textDataList = textDataList;
        }
        public TextDatas(string tag, int id, int index, int actionIndex, List<TextData> textDataList)
        {
            this.tag = tag;
            this.id = id;
            this.index = index;
            this.actionIndex = actionIndex;
            this.textDataList = textDataList;
        }

        public TextDatas(List<TextDatas> strDatas)
        {
            var isDup = false;
            do
            {
                this.id = Random.Range(0, int.MaxValue);
                foreach (var item in strDatas)
                {
                    if (this.id == item.id)
                    {
                        isDup = true;
                        break;
                    }
                }

            } while (isDup);
        }
    }

    [System.Serializable]
    public class TextData
    {
        public string language;
        public string text;
        public TextData(string language, string text)
        {
            this.language = language;
            this.text = text;
        }
    }



    string checkParent(Transform transform)
    {
        if (transform.parent == null || transform.parent.tag.Contains("cv_"))
            if (transform.parent != null)
                return transform.parent.tag;
            else
                return "cv_other";
        else
            return checkParent(transform.parent);
    }

    public int TUTORIAL_TEXT_INDEX = 0;
    public string TUTORIAL_TEXT_DATA = "";

    void Awake()
    {

        TUTORIAL_TEXT_INDEX = 0;

        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }

        //load textData
        stringData = new StringData();
        DataManager.loadData<StringData>("gameText", ref stringData);

        //Debug.Log(stringData.tutorialDatas.Count);


        TUTORIAL_TEXT_DATA = Resources.Load("gameText").ToString();


    }
    private void Start()
    {

        //Debug.Log("chapter: " + stringData.tutorialDatas.Count);
        for (int i = 0; i < stringData.tutorialDatas.Count; i++)
        {
            //Debug.Log("inside: " + stringData.tutorialDatas[i].tutsDatas.Count);
            var tutorial_texts = new List<Dictionary<string, string>>();
            for (int j = 0; j < stringData.tutorialDatas[i].tutsDatas.Count; j++)
            {
                var dict = new Dictionary<string, string>();
                foreach (var item in stringData.tutorialDatas[i].tutsDatas[j].textDataList)
                {
                    dict.Add(item.language, item.text);
                }
                tutorial_texts.Add(dict);


                //add to normal text data
                stringData.textDatas.Add(new TextDatas(
                    stringData.tutorialDatas[i].tutsDatas[j].tag,
                    stringData.tutorialDatas[i].tutsDatas[j].id,
                    stringData.tutorialDatas[i].tutsDatas[j].index,
                    stringData.tutorialDatas[i].tutsDatas[j].actionIndex,
                    stringData.tutorialDatas[i].tutsDatas[j].textDataList
                    ));
                //Debug.Log("added: " + stringData.tutorialDatas[i].tutsDatas[j].id);

            }
            //            Debug.Log(TutorialNew.instance.tutorial_texts.Count);
            TutorialNew.instance.tutorial_texts.Add(tutorial_texts);
        }

        List<TextReplacer> tr = new List<TextReplacer>();
        fetchTextReplacer(tr, this.transform);

        //Debug.Log("tr found : " + tr.Count);

        foreach (var repleacer in tr)
        {
            foreach (var textData in stringData.textDatas)
            {
                if (repleacer.id == textData.id)
                {
                    repleacer.stringTag = textData.tag;
                    if (textData.index != -1) repleacer.index = textData.index;
                    if (textData.actionIndex != -1) repleacer.actionIndex = textData.actionIndex;
                    foreach (var data in textData.textDataList)
                    {
                        try
                        {
                            repleacer.textDatas.Add(data.language, data.text);
                        }
                        catch (System.Exception)
                        {
                            // Debug.Log("Duplicate detected, skip: "+repleacer.id);
                            // throw;
                        }
                    }
                }
            }
        }


        UI_ReferenceManager.instance.CanvasStory.SetActive(true);

        //StoryMode sm = FindObjectOfType(typeof(StoryMode)) as StoryMode;
        StoryMode sm = UI_ReferenceManager.instance.CanvasStory.GetComponent<StoryMode>();
        foreach (var storypage in sm._StoryStore)
        {
            for (int i = 0; i < storypage._TextId.Count; i++)
            {
                var id = storypage._TextId[i];
                foreach (var textData in stringData.textDatas)
                {
                    if (id == textData.id)
                    {
                        foreach (var data in textData.textDataList)
                        {
                            storypage._TextData.Add(data.language, data.text);
                        }
                    }
                }
            }
        }

        this.ChangeLanguage();
        UI_ReferenceManager.instance.CanvasStory.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("test update");
    }

    public string getLangString()
    {

        var lanStr = "";
        switch (LanguageSettings.instance.gameMode)
        {
            case LanguageSettings.GameMode.Drug_City:
                switch (LanguageSettings.instance.language)
                {
                    case LanguageSettings.Language.Thai:
                        lanStr = "DRG_TH";
                        break;
                    case LanguageSettings.Language.English:
                        lanStr = "DRG_EN";
                        break;
                }
                break;
            case LanguageSettings.GameMode.Green_City:
                switch (LanguageSettings.instance.language)
                {
                    case LanguageSettings.Language.Thai:
                        lanStr = "GRN_TH";
                        break;
                    case LanguageSettings.Language.English:
                        lanStr = "GRN_EN";
                        break;
                }
                break;
        }
        return lanStr;
    }
    public void ChangeLanguage()
    {
        ChangeLanguage(getLangString());
    }

    public void ChangeLanguage(string lanStr)
    {

        List<TextReplacer> tr = new List<TextReplacer>();
        fetchTextReplacer(tr, this.transform);
        foreach (var repleacer in tr)
        {
            repleacer.ChangeLanguage(lanStr);
        }

        //StoryMode sm = FindObjectOfType(typeof(StoryMode)) as StoryMode;
        StoryMode sm = UI_ReferenceManager.instance.CanvasStory.GetComponent<StoryMode>();
        foreach (var storypage in sm._StoryStore)
        {
            storypage.ChangeLanguage(lanStr);
        }

        CardManager cm = FindObjectOfType<CardManager>() as CardManager;
        for (int i = 0; i < cm._AllCardStore.Count; i++)
        {
            var card = cm._AllCardStore[i];
            foreach (var nodeData in stringData.nodeDatas)
            {
                if (nodeData.index == i)
                {
                    switch (LanguageSettings.instance.language)
                    {
                        case LanguageSettings.Language.Thai:
                            card._CardName = nodeData.nodeDataList[0].text;
                            break;
                        case LanguageSettings.Language.English:
                            card._CardName = nodeData.nodeDataList[1].text;
                            break;
                    }
                }
            }
        }
    }

    int textCount = 0;
    void exportAllText()
    {
        getText(this.transform);
        //DataManager.saveData<StringData>(Application.dataPath + "/Resources", "strings", stringData);
        Debug.Log(textCount);
    }



    void fetchTextReplacer(List<TextReplacer> textReplacers, Transform transform)
    {
        if (transform.GetComponent<TextReplacer>())
        {
            textReplacers.Add(transform.GetComponent<TextReplacer>());
        }
        foreach (Transform item in transform)
        {
            fetchTextReplacer(textReplacers, item);
        }

    }

    void getText(Transform transform)
    {
        if (transform.gameObject.name != "Canvas-Story")
        {
            if (transform.GetComponent<Text>())
            {
                textCount++;
                parentText.Add(transform.GetComponent<Text>());
                var tempData = new TextDatas(stringData.textDatas);
                tempData.textDataList.Add(new TextData("TH", transform.GetComponent<Text>().text));
                tempData.textDataList.Add(new TextData("EN", "Text in EN for " + tempData.id));
                stringData.textDatas.Add(tempData);

                if (transform.GetComponent<TextReplacer>() == null)
                {
                    transform.gameObject.AddComponent<TextReplacer>();
                }
                transform.GetComponent<TextReplacer>().id = tempData.id;
            }
        }
        else
        {

            //Debug.Log("in story");

            StoryMode sm = transform.GetComponent<StoryMode>();

            foreach (var ss in sm._StoryStore)
            {
                foreach (var text in ss._Text)
                {
                    Debug.Log(text);
                    textCount++;
                    var tempData = new TextDatas(stringData.textDatas);
                    //tempData.textDataList.Add(new TextData("TH", text));
                    tempData.textDataList.Add(new TextData("EN", "Text in EN for " + tempData.id));
                    stringData.textDatas.Add(tempData);
                }
            }

        }
        foreach (Transform item in transform)
        {
            getText(item);
        }
    }

}
