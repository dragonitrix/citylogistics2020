﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SimpleShareSolutionText : MonoBehaviour
{
    [TextArea(10, 20)]
    public string drag;
    [TextArea(10, 20)]
    public string green;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<TextMeshProUGUI>().enabled = transform.parent.GetComponent<Image>().enabled;

        if (TextManager.instance.getLangString().Contains("DRG"))
        {
            GetComponent<TextMeshProUGUI>().text = drag;
        }

        if (TextManager.instance.getLangString().Contains("GRN"))
        {
            GetComponent<TextMeshProUGUI>().text = green;
        }
    }
}
