﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleImageOnLanguage : MonoBehaviour
{
    public Sprite drug;
    public Sprite green;
    // Start is called before the first frame update
    void Start()
    {
        var lanstr = TextManager.instance.getLangString();
        if (lanstr.Contains("DRG"))
        {
            GetComponent<Image>().sprite = drug;
        }
        else if (lanstr.Contains("GRN"))
        {
            GetComponent<Image>().sprite = green;
        }
    }
}
