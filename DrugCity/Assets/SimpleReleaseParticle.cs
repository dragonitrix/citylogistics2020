﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleReleaseParticle : MonoBehaviour
{
    public GameObject _ParticleAnim;

    SpriteRenderer sr;

    bool spriteStatus;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        spriteStatus = sr.enabled;
    }

    // Update is called once per frame
    void Update()
    {
        if (sr.sprite != null && spriteStatus && !sr.enabled)
        {
            var particle = Instantiate(_ParticleAnim, transform.position, transform.rotation, transform);

            SoundManager.instance.PlaySound("Green Energy", SoundType.SFX_1);
            Destroy(particle, 2);
        }
        spriteStatus = sr.enabled;
    }
}
