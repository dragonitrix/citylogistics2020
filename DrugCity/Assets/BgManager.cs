﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgManager : MonoBehaviour
{
    public static BgManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }


    public List<Sprite> goodSprite;
    public List<Sprite> badSprite;

    public int index = 0;

    public void setBG()
    {
        setBG(index + 1);
    }

    public void setBG(int index)
    {

        this.index = index;

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        for (int i = 0; i < this.index + 1; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var lanstr = TextManager.instance.getLangString();
            if (lanstr.Contains("DRG"))
            {
                transform.GetChild(i).GetComponent<SpriteRenderer>().sprite = badSprite[i];
            }
            else if (lanstr.Contains("GRN"))
            {
                transform.GetChild(i).GetComponent<SpriteRenderer>().sprite = goodSprite[i];
            }
            transform.GetChild(i).gameObject.SetActive(false);
        }

        setBG(0);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
