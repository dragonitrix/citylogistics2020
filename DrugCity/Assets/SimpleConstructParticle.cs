﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleConstructParticle : MonoBehaviour
{
    public GameObject _ParticleAnim;

    SpriteRenderer sr;

    Sprite sprite;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        sprite = sr.sprite;
    }

    // Update is called once per frame
    void Update()
    {
        if (sr.sprite != null && sprite != sr.sprite)
        {
            var particle = Instantiate(_ParticleAnim, transform.position, transform.rotation, transform);
            particle.transform.localPosition = new Vector3(0, -0.75f, 0);
            Destroy(particle, 0.5f);
        }
        sprite = sr.sprite;
    }
}
