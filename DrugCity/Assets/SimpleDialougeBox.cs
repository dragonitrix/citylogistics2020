﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class SimpleDialougeBox : MonoBehaviour
{

    public List<GameObject> objToHide = new List<GameObject>();

    [TextArea(10, 20)]
    public string drug;
    [TextArea(10, 20)]
    public string green;

    string[] texts;

    int index = 0;

    private void OnEnable()
    {
        init();
    }

    private void Start()
    {
        init();
    }

    void init()
    {

        var lanstr = TextManager.instance.getLangString();
        if (lanstr.Contains("DRG"))
        {
            texts = drug.Split(new string[] { "${split}" }, StringSplitOptions.RemoveEmptyEntries);
        }
        else if (lanstr.Contains("GRN"))
        {
            texts = green.Split(new string[] { "${split}" }, StringSplitOptions.RemoveEmptyEntries);
        }
        index = 0;
        setText(texts[index]);
    }
    void setText(string text)
    {
        text = text.Replace("\n", "");
        text = text.Replace("\r", "");
        transform.GetComponentInChildren<TextMeshProUGUI>().text = text;
    }

    public void onNextClicked()
    {
        index++;
        if (index < texts.Length)
        {
            setText(texts[index]);
        }
        else
        {
            //end
            foreach (var item in objToHide)
            {
                item.SetActive(false);
            }
        }
    }

    private void Update()
    {

    }

}
