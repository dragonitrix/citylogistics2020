﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using System.IO;

public class TextReplacer : MonoBehaviour
{
    public int id;
    public int index = -1;
    public int actionIndex = -1;
    public string stringTag = "";

    // Dictionary<string,string> textData = new Dictionary<string, string>();

    public Dictionary<string, string> textDatas = new Dictionary<string, string>();

    void SetAndStretchToParentSize(RectTransform _mRect, RectTransform _parent)

    {
        // _mRect.anchoredPosition = _parent.position;
        _mRect.anchorMin = new Vector2(0, 0);
        _mRect.anchorMax = new Vector2(1, 1);
        _mRect.pivot = new Vector2(0.5f, 0.5f);
        _mRect.sizeDelta = _parent.rect.size;
        _mRect.transform.SetParent(_parent);
    }


    void SwapToTmp()
    {
        var text = this.GetComponent<Text>();

        var fontSize = text.fontSize;
        var color = text.color;
        /*
        TextAlignmentOptions alignment = TextAlignmentOptions.MidlineJustified;
        switch (text.alignment)
        {
            case TextAnchor.UpperLeft:
                alignment = TextAlignmentOptions.TopLeft;
                break;
            case TextAnchor.UpperCenter:
                alignment = TextAlignmentOptions.TopJustified;
                break;
            case TextAnchor.UpperRight:
                alignment = TextAlignmentOptions.TopRight;
                break;
            case TextAnchor.MiddleLeft:
                alignment = TextAlignmentOptions.MidlineLeft;
                break;
            case TextAnchor.MiddleCenter:
                alignment = TextAlignmentOptions.MidlineJustified;
                break;
            case TextAnchor.MiddleRight:
                alignment = TextAlignmentOptions.MidlineRight;
                break;
            case TextAnchor.LowerLeft:
                alignment = TextAlignmentOptions.BaselineLeft;
                break;
            case TextAnchor.LowerCenter:
                alignment = TextAlignmentOptions.BaselineJustified;
                break;
            case TextAnchor.LowerRight:
                alignment = TextAlignmentOptions.BaselineRight;
                break;
            default:
                break;
        }
        */

        text.enabled = false;

        var tmp_obj = new GameObject();

        tmp_obj.transform.SetParent(transform, false);

        tmp_obj.AddComponent<RectTransform>();

        SetAnchor(tmp_obj.GetComponent<RectTransform>(), AnchorPresets.StretchAll, 0, 0);
        SetPivot(tmp_obj.GetComponent<RectTransform>(), PivotPresets.MiddleCenter);

        tmp_obj.GetComponent<RectTransform>().offsetMin = Vector2.zero;
        tmp_obj.GetComponent<RectTransform>().offsetMax = Vector2.zero;

        //SetAndStretchToParentSize(tmp_obj.GetComponent<RectTransform>(), transform.GetComponent<RectTransform>());

        var minFontSize = 25f;

        var tmp = tmp_obj.AddComponent<TextMeshProUGUI>();
        tmp.fontStyle = FontStyles.Bold;
        tmp.text = text.text;
        tmp.font = TextManager.instance.fontasset;
        //tmp.fontSize = Mathf.Clamp(text.fontSize, minFontSize, text.fontSize);
        tmp.fontSize = minFontSize;

        tmp.enableAutoSizing = true;
        tmp.fontSizeMax = 25;
        tmp.fontSizeMin = 10;

        tmp.color = color;
        //tmp.alignment = alignment;
        tmp.alignment = TextAlignmentOptions.MidlineLeft;
        tmp.enableWordWrapping = true;

        tmp.ForceMeshUpdate();

        //Debug.Log(tmp.textBounds.size);
        //
        //if (tmp.textBounds.size.y > tmp_obj.GetComponent<RectTransform>().sizeDelta.y)
        //{
        //    Debug.Log("overflow on:" + name);
        //}
        //
        //if (tmp.isTextOverflowing) Debug.Log(tmp.isTextOverflowing);
        //while (tmp.isTextOverflowing && tmp.fontSize > 0)
        //{
        //    tmp.fontSize--;
        //}
        //
        //if (tmp.isTextTruncated) Debug.Log(tmp.isTextTruncated);
        //while (tmp.isTextTruncated && tmp.fontSize > 0)
        //{
        //    tmp.fontSize--;
        //}

        //if (text.text.Contains("${"))
        //{
        //    var index = text.text.IndexOf("${");
        //    var iconindex = (int)text.text[index + 1];
        //    var pos = tmp.textInfo.characterInfo[index].bottomLeft;
        //
        //    GameObject icon = new GameObject();
        //    icon.AddComponent<RectTransform>();
        //    icon.AddComponent<Image>();
        //    icon.transform.position = pos;
        //    icon.transform.SetParent(transform);
        //
        //
        //}




    }

    private void OnEnable()
    {

        //var tmp = transform.GetComponentInChildren<TextMeshProUGUI>();
        //if (tmp == null) return;
        //tmp.ForceMeshUpdate();
        //
        //if (tmp.textBounds.size.y > tmp.transform.GetComponent<RectTransform>().sizeDelta.y)
        //{
        //    //Debug.Log("overflow on:" + name);
        //}
        //
        //while ((tmp.isTextOverflowing || tmp.isTextTruncated) && tmp.fontSize > 0)
        //{
        //    tmp.fontSize--;
        //    tmp.ForceMeshUpdate();
        //}

        if (stringTag == "cv_other")
        {
            ChangeLanguage(TextManager.instance.getLangString());
        }

    }



    private void Start()
    {
        //Debug.Log(transform.parent.gameObject);
        checkSplit();

        if (stringTag == "cv_Tut" && index != -1)
        {
            Debug.Log("Tut index : " + index + " Tut Step: " + GameObject.Find("Canvas").GetComponent<TutorialScript>()._Step);
        }

        if (stringTag == "cv_Tut")
        {
            if (actionIndex == 99)
            {
                if (transform.parent.GetComponentInChildren<Button>() != null)
                {
                    var nextButton = transform.parent.GetComponentInChildren<Button>();
                    nextButton.onClick.RemoveAllListeners();
                    nextButton.onClick = new Button.ButtonClickedEvent();
                    nextButton.onClick.AddListener(() =>
                    {
                        //nextButton.transform.parent.gameObject.SetActive(false);
                        TutorialNew.instance.ResetTutorial();
                    });
                }
                else
                {
                    TutorialNew.instance.ResetTutorial();

                }
            }
        }


        //if (stringTag != "cv_Tut") return;
        //if (transform.GetComponentInChildren<TextMeshProUGUI>() == null) return;
        //
        //var tmp = transform.GetComponentInChildren<TextMeshProUGUI>();
        //tmp.enableAutoSizing = true;
        //tmp.fontSizeMax = 25;
        //tmp.fontSizeMin = 10;

    }

    public void checkSplit()
    {
        if (stringTag != "cv_Tut") return;
        if (transform.GetComponentInChildren<TextMeshProUGUI>() == null) return;

        var text = transform.GetComponentInChildren<TextMeshProUGUI>().text;
        //smart split
        if (text.Contains("${split}"))
        {
            var texts = text.Split(new string[] { "${split}" }, StringSplitOptions.RemoveEmptyEntries);

            var leftOvertext = "";

            for (int i = 0; i < texts.Length; i++)
            {
                if (i == 0)
                {
                    text = texts[0];
                    setText(text);
                    continue;
                }
                leftOvertext += texts[i];
                if (i < texts.Length - 1) leftOvertext += "${split}";
            }
            //Debug.Log(leftOvertext);
            createClone(leftOvertext);
        }
    }

    public void createClone(string text)
    {


        var obj = transform.parent.gameObject;
        var clone = Instantiate(transform.parent.gameObject, transform.parent.parent);
        clone.SetActive(false);

        clone.GetComponentInChildren<TextReplacer>().setText(text);

        var thisButton = obj.GetComponentInChildren<Button>();
        var cloneButton = obj.GetComponentInChildren<Button>(); //??

        cloneButton.onClick = thisButton.onClick;
        thisButton.onClick = new Button.ButtonClickedEvent();
        thisButton.onClick.AddListener(() =>
        {
            obj.SetActive(false);
            clone.SetActive(true);
        });

    }

    public string getText()
    {
        var text = "";

        if (transform.GetComponentInChildren<TextMeshProUGUI>() == null)
        {
            text = this.GetComponent<Text>().text;
        }
        else
        {
            text = transform.GetComponentInChildren<TextMeshProUGUI>().text;
        }

        return text;
    }

    public void setText(string text)
    {
        if (transform.GetComponentInChildren<TextMeshProUGUI>() == null)
        {
            this.GetComponent<Text>().text = text;
            this.SwapToTmp();
        }
        else
        {
            transform.GetComponentInChildren<TextMeshProUGUI>().text = text;
        }
    }

    public void ChangeLanguage(string lang)
    {
        var result = "";
        if (textDatas.TryGetValue(lang, out result))
        {
            if (this.stringTag == "cv_Dialouge" || this.stringTag == "cv_Tut" || this.stringTag == "cv_Role" )
            {
                //setText(result + "_" + id);
                setText(result);

            }
        }
        else
        {
        }
        //Debug.Log(lang+" "+ id);
        /*
        var result = "";
        if (textDatas.TryGetValue(lang, out result))
        {
            this.GetComponent<Text>().text = result;
            this.SwapToTmp();
            //transform.GetComponentInChildren<TextMeshProUGUI>().text = result;
        }
        else
        {
            //Debug.Log("target language is null. use default instead :");
        } */
    }

    string FindLanguage(string lang)
    {
        foreach (var item in textDatas)
        {
            if (item.Key == lang) return item.Value;
        }
        return null;
    }



    public enum AnchorPresets
    {
        TopLeft,
        TopCenter,
        TopRight,

        MiddleLeft,
        MiddleCenter,
        MiddleRight,

        BottomLeft,
        BottonCenter,
        BottomRight,
        BottomStretch,

        VertStretchLeft,
        VertStretchRight,
        VertStretchCenter,

        HorStretchTop,
        HorStretchMiddle,
        HorStretchBottom,

        StretchAll
    }

    public enum PivotPresets
    {
        TopLeft,
        TopCenter,
        TopRight,

        MiddleLeft,
        MiddleCenter,
        MiddleRight,

        BottomLeft,
        BottomCenter,
        BottomRight,
    }

    public void SetAnchor(RectTransform source, AnchorPresets allign, int offsetX = 0, int offsetY = 0)
    {
        source.anchoredPosition = new Vector3(offsetX, offsetY, 0);

        switch (allign)
        {
            case (AnchorPresets.TopLeft):
                {
                    source.anchorMin = new Vector2(0, 1);
                    source.anchorMax = new Vector2(0, 1);
                    break;
                }
            case (AnchorPresets.TopCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 1);
                    source.anchorMax = new Vector2(0.5f, 1);
                    break;
                }
            case (AnchorPresets.TopRight):
                {
                    source.anchorMin = new Vector2(1, 1);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }

            case (AnchorPresets.MiddleLeft):
                {
                    source.anchorMin = new Vector2(0, 0.5f);
                    source.anchorMax = new Vector2(0, 0.5f);
                    break;
                }
            case (AnchorPresets.MiddleCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 0.5f);
                    source.anchorMax = new Vector2(0.5f, 0.5f);
                    break;
                }
            case (AnchorPresets.MiddleRight):
                {
                    source.anchorMin = new Vector2(1, 0.5f);
                    source.anchorMax = new Vector2(1, 0.5f);
                    break;
                }

            case (AnchorPresets.BottomLeft):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(0, 0);
                    break;
                }
            case (AnchorPresets.BottonCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 0);
                    source.anchorMax = new Vector2(0.5f, 0);
                    break;
                }
            case (AnchorPresets.BottomRight):
                {
                    source.anchorMin = new Vector2(1, 0);
                    source.anchorMax = new Vector2(1, 0);
                    break;
                }

            case (AnchorPresets.HorStretchTop):
                {
                    source.anchorMin = new Vector2(0, 1);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }
            case (AnchorPresets.HorStretchMiddle):
                {
                    source.anchorMin = new Vector2(0, 0.5f);
                    source.anchorMax = new Vector2(1, 0.5f);
                    break;
                }
            case (AnchorPresets.HorStretchBottom):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(1, 0);
                    break;
                }

            case (AnchorPresets.VertStretchLeft):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(0, 1);
                    break;
                }
            case (AnchorPresets.VertStretchCenter):
                {
                    source.anchorMin = new Vector2(0.5f, 0);
                    source.anchorMax = new Vector2(0.5f, 1);
                    break;
                }
            case (AnchorPresets.VertStretchRight):
                {
                    source.anchorMin = new Vector2(1, 0);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }

            case (AnchorPresets.StretchAll):
                {
                    source.anchorMin = new Vector2(0, 0);
                    source.anchorMax = new Vector2(1, 1);
                    break;
                }
        }
    }

    public void SetPivot(RectTransform source, PivotPresets preset)
    {

        switch (preset)
        {
            case (PivotPresets.TopLeft):
                {
                    source.pivot = new Vector2(0, 1);
                    break;
                }
            case (PivotPresets.TopCenter):
                {
                    source.pivot = new Vector2(0.5f, 1);
                    break;
                }
            case (PivotPresets.TopRight):
                {
                    source.pivot = new Vector2(1, 1);
                    break;
                }

            case (PivotPresets.MiddleLeft):
                {
                    source.pivot = new Vector2(0, 0.5f);
                    break;
                }
            case (PivotPresets.MiddleCenter):
                {
                    source.pivot = new Vector2(0.5f, 0.5f);
                    break;
                }
            case (PivotPresets.MiddleRight):
                {
                    source.pivot = new Vector2(1, 0.5f);
                    break;
                }

            case (PivotPresets.BottomLeft):
                {
                    source.pivot = new Vector2(0, 0);
                    break;
                }
            case (PivotPresets.BottomCenter):
                {
                    source.pivot = new Vector2(0.5f, 0);
                    break;
                }
            case (PivotPresets.BottomRight):
                {
                    source.pivot = new Vector2(1, 0);
                    break;
                }
        }
    }
}
