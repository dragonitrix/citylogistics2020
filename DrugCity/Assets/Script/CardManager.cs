using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class CardManager : MonoBehaviour
{
    [Header("Add Card Here")]
    public List<_Card> _AllCardStore = new List<_Card>();

    [Header("Infection Card")]
    public List<_Card> _InfectionPile = new List<_Card>();
    public List<_Card> _Discardplie = new List<_Card>();


    [Header("City Card")]
    public List<_Card> _AllPLayerCardPile = new List<_Card>();
    public List<_Card> _PLayerDiscardplie = new List<_Card>();

    public int _CardPickRate;

    [System.Serializable]
    public class _Card
    {
        public string _CardName;
        public bool _isEpidemics;
        public GameObject _Node;
    }
    int _Random;
    int _RandomEpi;
    public int _Plus;
    public Text _CityCardCount;

    void Start()
    {
        Application.targetFrameRate = 30;
    }

    private void Update()
    {
        //lazy share solution fix
        if (_ReportTxt.text == "Share solution")
        {
            _ReportPopUp.transform.Find("Share").GetComponent<Image>().enabled = true;
        }
        else
        {
            _ReportPopUp.transform.Find("Share").GetComponent<Image>().enabled = false;
        }
    }

    public void _RandomCard()
    {

        _ReportTxt.text = "Orders Report";

        if (GetComponent<TutorialScript>()._isOnTutorial)
        {
            if (GetComponent<TutorialScript>()._Step == 4)
            {
                // GetComponent<TutorialScript>()._ShowPopUp();
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node09)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node08)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                GetComponent<TutorialScript>()._StepUp();

                GetComponent<TutorialScript>()._ExtraPopUp[7].SetActive(false);
                GetComponent<TutorialScript>()._ExtraPopUp[8].SetActive(true);
            }



            if (GetComponent<TutorialScript>()._Step == 9)
            {
                // GetComponent<TutorialScript>()._ShowPopUp();
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._SecondWave[0])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._SecondWave[1])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                //GetComponent<TutorialScript> ()._Tutorial_MustShow [5].SetActive (false);


            }

            if (GetComponent<TutorialScript>()._Step == 7)
            {
                // GetComponent<TutorialScript>()._ShowPopUp();
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._SecondWave[0])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 3;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._SecondWave[1])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 2;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
            }

            if (GetComponent<TutorialScript>()._Step == 13)
            {
                // GetComponent<TutorialScript>()._ShowPopUp();
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._12WaveSlot[0])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                _PanelTutorial06.SetActive(true);

            }

            if (GetComponent<TutorialScript>()._Step == 15)
            {
                // GetComponent<TutorialScript>()._ShowPopUp();
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node10)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node01)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node06)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                GetComponent<TutorialScript>()._StepUp();
            }

            if (GetComponent<TutorialScript>()._Step == 19)
            {
                // GetComponent<TutorialScript>()._ShowPopUp();
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node09)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node14)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node18)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                GetComponent<TutorialScript>()._StepUp();
            }

            if (GetComponent<TutorialScript>()._Step == 24)
            {
                // GetComponent<TutorialScript>()._ShowPopUp();
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node09)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node10)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node22)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                //GetComponent<TutorialScript> ()._Step++;
            }

            if (GetComponent<TutorialScript>()._Step == 27)
            {
                // GetComponent<TutorialScript>()._ShowPopUp();
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node09)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node21)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node23)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                GetComponent<TutorialScript>()._StepUp();
            }

            if (GetComponent<TutorialScript>()._Step == 31)
            {
                // GetComponent<TutorialScript>()._ShowPopUp();
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node01)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node14)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node18)
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }

                GetComponent<TutorialScript>()._StepUp();
            }


            _InfectionBanner();
            _NodeManger.GetComponent<NodeManager>()._Reset();
            PlayerScript.Instance._ActionBtm[7].interactable = false;
        }
        else if (GetComponent<Tutorial_Section02>()._Section02)
        {
            if (GetComponent<Tutorial_Section02>()._Step == 3)
            {
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[4])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[9])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
            }

            if (GetComponent<Tutorial_Section02>()._Step == 7)
            {
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[5])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[7])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
            }

            if (GetComponent<Tutorial_Section02>()._Step == 11)
            {
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[6])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[10])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
            }

            if (GetComponent<Tutorial_Section02>()._Step == 14)
            {
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[1])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[5])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[23])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
            }

            if (GetComponent<Tutorial_Section02>()._Step == 18)
            {
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[4])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[20])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[22])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
            }

            if (GetComponent<Tutorial_Section02>()._Step == 22)
            {
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[12])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[22])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
                foreach (_Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[23])
                    {
                        InfectionEvent _Event = new InfectionEvent();
                        _Event._InfectionLevel = 1;
                        _Event._Node = x._Node;
                        _InfectionStore.Add(_Event);
                        break;
                    }
                }
            }

            _InfectionBanner();
            _NodeManger.GetComponent<NodeManager>()._Reset();
        }
        else
        {
            for (int i = 0; i < _CardPickRate; i++)
            {
                // _Report(_InfectionPile[0], "Infection");

                //PlayerScript.Instance._DeInfection (_InfectionPile [0]._Node.GetComponent<NodeScript> ()._Type);
                //_InfectionPile[0]._Node.GetComponent<NodeScript>()._LevelUp(1, _InfectionPile[0]._Node.GetComponent<NodeScript>()._Type); // Take First Card on Pile and Add 1 Cube.
                InfectionEvent _Event = new InfectionEvent();
                _Event._InfectionLevel = 1;
                _Event._Node = _InfectionPile[0]._Node;

                _InfectionStore.Add(_Event);

                _Discardplie.Add(_InfectionPile[0]); // Add Card to Discard Pile 
                _InfectionPile.RemoveAt(0); // Remove Card from Infection Pile

            }

            _InfectionBanner();

            _NodeManger.GetComponent<NodeManager>()._Reset();
        }
    }

    public List<InfectionEvent> _InfectionStore = new List<InfectionEvent>();
    [System.Serializable]
    public class InfectionEvent
    {
        public int _InfectionLevel;
        public GameObject _Node;
        public bool _isOutBreak;
    }

    public GameObject _Panel;
    public Text _InfectText;
    public GameObject _PoliceImage;
    public Image _ImageType;
    public GameObject _Next;

    public GameObject _PanelTutorial08;
    public void _ResetNode()
    {
        foreach (GameObject i in _NodeManger.GetComponent<NodeManager>()._Node)
        {
            i.GetComponent<NodeScript>()._isOutBreak = false;
        }
        //_NodeManger.GetComponent<NodeManager>()._Node
    }

    public void _InfectionBanner()
    {
        _PoliceImage.SetActive(false);
        //_InfectionProgress ();
        if (_InfectionStore.Count > 0)
        {
            if (_InfectionStore[0]._isOutBreak)
            {
                _InfectText.text = _InfectionStore[0]._Node.GetComponent<NodeScript>()._NodeName + " has extreme congestion level.";

                _PoliceImage.SetActive(true);
                SoundManager.instance.StopSound(SoundType.SFX_2);
                SoundManager.instance.PlaySound("Siren", SoundType.SFX_2);

            }
            else if (PlayerScript.Instance._CurrectNodePlayerOne == _InfectionStore[0]._Node && PlayerScript.Instance._PlayerOneClass == 0)
            {
                _InfectText.text = _InfectionStore[0]._Node.GetComponent<NodeScript>()._NodeName + " is prevented by Retailer";
            }
            else if (PlayerScript.Instance._CurrectNodePlayerTwo == _InfectionStore[0]._Node && PlayerScript.Instance._PlayerTwoClass == 0)
            {
                _InfectText.text = _InfectionStore[0]._Node.GetComponent<NodeScript>()._NodeName + " is prevented by Retailer";
            }
            else
            {

                if (_InfectionStore[0]._Node.GetComponent<NodeScript>()._Type == 1)
                {
                    if (!PlayerScript.Instance._isCureType[1])
                    {
                        _InfectText.text = "Orders are dispatching to " + _InfectionStore[0]._Node.GetComponent<NodeScript>()._NodeName;

                    }
                    else
                    {
                        _InfectText.text = "The traffic problems in " + _InfectionStore[0]._Node.GetComponent<NodeScript>()._NodeName + " are solved.\nTraffic congestion and air pollutions caused by Red manufacturer has been solved.";
                    }

                    // The traffic problems  in ชื่อถนน are solved.Traffic congestion and air pollutions caused by(Red หรือ Blue) manufacturer has been solved.

                    /* The traffic problems  in ชื่อถนน are solved.
 Traffic congestion and air pollutions caused by(Red หรือ Blue) manufacturer has been solved.*/
                }
                else
                {
                    if (!PlayerScript.Instance._isCureType[0])
                    {
                        _InfectText.text = "Orders are dispatching to " + _InfectionStore[0]._Node.GetComponent<NodeScript>()._NodeName;
                    }
                    else
                    {
                        _InfectText.text = "The traffic problems in " + _InfectionStore[0]._Node.GetComponent<NodeScript>()._NodeName + " are solved.\nTraffic congestion and air pollutions caused by Blue manufacturer has been solved.";
                    }
                }

            }



            if (GetComponent<TutorialScript>()._Step == 9)
            {
                GetComponent<TutorialScript>()._StepUp();
                GetComponent<TutorialScript>()._ShowPopUp();
                GetComponent<TutorialScript>()._StepUp();
            }
            _Panel.SetActive(true);
        }
        else
        {



            if (GetComponent<TutorialScript>()._isOnTutorial)
            {
                if (GetComponent<TutorialScript>()._Step == 0)
                {
                    GetComponent<TutorialScript>()._ShowPopUp(6);
                }

                if (GetComponent<TutorialScript>()._Step == 5)
                {
                    // ExtraInfectionForTutorial();
                    GetComponent<TutorialScript>()._ExtraPopUp[8].SetActive(false);
                    GetComponent<TutorialScript>()._ShowPopUp();
                }

                if (GetComponent<TutorialScript>()._Step == 8)
                {
                    GetComponent<TutorialScript>()._ShowPopUp();

                }

                if (GetComponent<TutorialScript>()._Step == 11)
                {
                    GetComponent<TutorialScript>()._ShowPopUp();
                    GetComponent<TutorialScript>()._StepUp();
                }

                if (GetComponent<TutorialScript>()._Step == 16)
                {
                    GetComponent<TutorialScript>()._ShowPopUp();
                    //GetComponent<TutorialScript> ()._Step++;
                }


                if (GetComponent<TutorialScript>()._Step == 20)
                {
                    GetComponent<TutorialScript>()._ShowPopUp();
                    //GetComponent<TutorialScript> ()._Step++;
                }

                if (GetComponent<TutorialScript>()._Step == 24)
                {
                    GetComponent<TutorialScript>()._ShowPopUp();
                    //GetComponent<TutorialScript> ()._Step++;
                }

                if (GetComponent<TutorialScript>()._Step == 28)
                {
                    GetComponent<TutorialScript>()._ShowPopUp();
                    //GetComponent<TutorialScript> ()._Step++;
                }


                if (GetComponent<TutorialScript>()._Step == 32)
                {
                    GetComponent<TutorialScript>()._ShowPopUp();
                    GetComponent<TutorialScript>()._StepUp();
                }





                PlayerScript.Instance.TopStatBar.SetActive(false);

                _Panel.SetActive(false);


                if (GetComponent<TutorialScript>()._Step != 0)
                {
                    if (GetComponent<TutorialScript>()._Step != 12)
                    {
                        PlayerScript.Instance._CheckCard();
                        PlayerScript.Instance._PlayState = 0;

                        PlayerScript.Instance._ChangeImage();
                    }
                }

            }
            else if (GetComponent<Tutorial_Section02>()._Section02)
            {

                if (GetComponent<Tutorial_Section02>()._Step == 0)
                {
                    GetComponent<Tutorial_Section02>()._Tu[0].SetActive(true);
                }

                if (GetComponent<Tutorial_Section02>()._Step == 3)
                {
                    GetComponent<Tutorial_Section02>()._Tu[4].SetActive(true);
                    GetComponent<Tutorial_Section02>()._StepUp();
                }

                if (GetComponent<Tutorial_Section02>()._Step == 7)
                {
                    GetComponent<Tutorial_Section02>()._Tu[7].SetActive(true);
                    GetComponent<Tutorial_Section02>()._StepUp();
                }

                if (GetComponent<Tutorial_Section02>()._Step == 11)
                {
                    GetComponent<Tutorial_Section02>()._Tu[8].SetActive(true);
                    //GetComponent<Tutorial_Section02> ()._Step++;
                }

                if (GetComponent<Tutorial_Section02>()._Step == 14)
                {
                    GetComponent<Tutorial_Section02>()._Tu[10].SetActive(true);
                    GetComponent<Tutorial_Section02>()._StepUp();
                }

                if (GetComponent<Tutorial_Section02>()._Step == 18)
                {
                    GetComponent<Tutorial_Section02>()._Tu[12].SetActive(true);
                    GetComponent<Tutorial_Section02>()._StepUp();
                }

                if (GetComponent<Tutorial_Section02>()._Step == 22)
                {
                    GetComponent<Tutorial_Section02>()._Tu[13].SetActive(true);
                    GetComponent<Tutorial_Section02>()._StepUp();
                }

                PlayerScript.Instance.TopStatBar.SetActive(false);

                _Panel.SetActive(false);

                if (GetComponent<Tutorial_Section02>()._Step != 0)
                {
                    PlayerScript.Instance._CheckCard();
                    PlayerScript.Instance._PlayState = 0;
                    PlayerScript.Instance._ChangeImage();
                }

            }
            else
            {
                PlayerScript.Instance._CheckCard();
                PlayerScript.Instance._PlayState = 0;

                PlayerScript.Instance._ChangeImage();

                PlayerScript.Instance.TopStatBar.SetActive(false);

                _Panel.SetActive(false);

            }
        }
    }

    public GameObject _PanelTutorial;
    public GameObject _PanelTutorial01;
    public GameObject _PanelTutorial04;
    public void _InfectionProgress()
    {




        if (_InfectionStore.Count > 0)
        {
            if (_InfectionStore[0]._isOutBreak)
            {
                _InfectionStore[0]._Node.GetComponent<NodeScript>()._OutBreak();

                /*if (GetComponent<TutorialScript>()._isOnTutorial)
                {
                    GetComponent<TutorialScript>()._ShowPopUp();
                }*/

            }
            else
            {
                if (PlayerScript.Instance._CurrectNodePlayerOne == _InfectionStore[0]._Node && PlayerScript.Instance._PlayerOneClass == 0)
                {
                    //_InfectText.text = "Medic here : " + _InfectionStore[0]._Node.GetComponent<NodeScript>().name + "is save";
                }
                else if (PlayerScript.Instance._CurrectNodePlayerTwo == _InfectionStore[0]._Node && PlayerScript.Instance._PlayerTwoClass == 0)
                {
                    // _InfectText.text = "Medic here : " + _InfectionStore[0]._Node.GetComponent<NodeScript>().name + "is save";
                }
                else if (_InfectionStore[0]._Node.GetComponent<NodeScript>()._Type == 1)
                {
                    if (!PlayerScript.Instance._isCureType[1])
                    {
                        _InfectionStore[0]._Node.GetComponent<NodeScript>()._LevelUp(_InfectionStore[0]._InfectionLevel, _InfectionStore[0]._Node.GetComponent<NodeScript>()._Type);
                    }
                    else
                    {
                        // _InfectText.text = "You already have cure for this.";
                    }
                }
                else
                {
                    if (!PlayerScript.Instance._isCureType[0])
                    {
                        _InfectionStore[0]._Node.GetComponent<NodeScript>()._LevelUp(_InfectionStore[0]._InfectionLevel, _InfectionStore[0]._Node.GetComponent<NodeScript>()._Type);
                    }
                    else
                    {
                        //_InfectText.text = "You already have cure for this.";
                    }
                }


            }

            _InfectionStore.RemoveAt(0);
        }

        _InfectionBanner();
    }

    public void _PickCard()
    { // Pick City Card

        _ReportTxt.text = "Picked Card";

        _isEpicEvent = false;
        if (GetComponent<TutorialScript>()._isOnTutorial)
        {
            if (GetComponent<TutorialScript>()._Step == 3)
            {
                //GetComponent<TutorialScript> ()._ShowPopUp ();

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node19)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node21)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                GetComponent<TutorialScript>()._ExtraPopUp[5].SetActive(false);


            }

            if (GetComponent<TutorialScript>()._Step == 9)
            {
                ///GetComponent<TutorialScript>()._ShowPopUp();

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node08)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                _EpicEvent();

                //GetComponent<TutorialScript> ()._Tutorial_MustShow [3].SetActive (false);

            }

            if (GetComponent<TutorialScript>()._Step == 12)
            {
                _EpicEvent();
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._12WaveSlot[2])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }
            }

            if (GetComponent<TutorialScript>()._Step == 8)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._PlayerB_StarterNode[2])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._PlayerB_StarterNode[3])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                //GetComponent<PlayerScript> ()._PhasePanel.SetActive (true);
                //GetComponent<PlayerScript> ()._PhaseSubPanel [3].SetActive (true);

            }

            if (GetComponent<TutorialScript>()._Step == 7)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._7WaveSlot[0])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._7WaveSlot[1])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }


                //GetComponent<PlayerScript> ()._PhasePanel.SetActive (true);
                //GetComponent<PlayerScript> ()._PhaseSubPanel [3].SetActive (true);

            }

            if (GetComponent<TutorialScript>()._Step == 15)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node16)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node20)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
                GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);
            }

            if (GetComponent<TutorialScript>()._Step == 19)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node15)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node18)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
                GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);
            }

            if (GetComponent<TutorialScript>()._Step == 24)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node17)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                _EpicEvent();
            }

            if (GetComponent<TutorialScript>()._Step == 27)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node09)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node02)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                //GetComponent<PlayerScript> ()._PhasePanel.SetActive (true);
                //GetComponent<PlayerScript> ()._PhaseSubPanel [3].SetActive (true);
            }

            if (GetComponent<TutorialScript>()._Step == 31)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node23)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._Node14)
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
                GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);
            }


            PlayerScript.Instance._PlayState = 2;
            //PlayerScript.Instance._PhaseSubPanel[3].SetActive(true);
            PlayerScript.Instance._CheckCardOnHand();

        }
        else if (GetComponent<Tutorial_Section02>()._Section02)
        {
            if (GetComponent<Tutorial_Section02>()._Step == 3)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[18])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[19])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }
                GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
                GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);

            }

            if (GetComponent<Tutorial_Section02>()._Step == 7)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[8])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[15])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }
                GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
                GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);
            }

            if (GetComponent<Tutorial_Section02>()._Step == 11)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[11])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[6])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }
                GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
                GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);
            }

            if (GetComponent<Tutorial_Section02>()._Step == 14)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[12])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                _EpicEvent();
                _isEpicEvent = true;
            }

            if (GetComponent<Tutorial_Section02>()._Step == 18)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[4])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[17])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }
                GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
                GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);
            }

            if (GetComponent<Tutorial_Section02>()._Step == 22)
            {
                foreach (_Card x in _AllPLayerCardPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[21])
                    {
                        _Report(x, " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        _AllPLayerCardPile.Remove(x);
                        break;
                    }
                }

                _EpicEvent();
            }

            PlayerScript.Instance._PlayState = 2;

            PlayerScript.Instance._CheckCardOnHand();
        }
        else
        {
            _count = 0;
            for (int i = 0; i < 2; i++)
            {
                if (PlayerScript.Instance._State == 1)
                {
                    if (!_AllPLayerCardPile[0]._isEpidemics)
                    {
                        _count++;
                        _Report(_AllPLayerCardPile[0], " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(_AllPLayerCardPile[0]); // Add Card to Player.
                        _AllPLayerCardPile.Remove(_AllPLayerCardPile[0]); // Remove Card from Pile.
                        _isEpicEvent = false;
                    }
                    else
                    {
                        _count++;
                        Debug.Log("Epic");
                        _EpicEvent();
                        _isEpicEvent = true;
                        break;
                    }
                }
                else
                {
                    if (!_AllPLayerCardPile[0]._isEpidemics)
                    {
                        _count++;
                        _Report(_AllPLayerCardPile[0], " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(_AllPLayerCardPile[0]);
                        _AllPLayerCardPile.Remove(_AllPLayerCardPile[0]);
                        _isEpicEvent = false;
                    }
                    else
                    {
                        _count++;
                        Debug.Log("Epic");
                        _EpicEvent();
                        _isEpicEvent = true;
                        break;
                    }
                }
            }


            if (_count >= 2)
            {
                PlayerScript.Instance._PlayState = 2;
                PlayerScript.Instance._CheckCardOnHand();

            }
        }

        //_CityCardCount.text = "Have "+_AllPLayerCardPile.Count +" Cards lefts.";
        _CityCardOnReport.gameObject.SetActive(true);
        //_CityCardOnReport.text = "Player deck have " + _AllPLayerCardPile.Count + " cards lefts.";

        _CityCardCount.text = _AllPLayerCardPile.Count + "";
        _CityCardOnReport.text = _AllPLayerCardPile.Count + "";
    }

    public bool _isEpicEvent;
    public int _count;

    public Text _CityCardOnReport;

    public void _PickOneCard()
    {

        if (!GetComponent<TutorialScript>()._isOnTutorial && !GetComponent<Tutorial_Section02>()._Section02)
        {
            if (_isEpicEvent && _count < 2)
            {
                if (PlayerScript.Instance._State == 1)
                {


                    if (!_AllPLayerCardPile[0]._isEpidemics)
                    {

                        _Report(_AllPLayerCardPile[0], " has been picked.");
                        PlayerScript.Instance._PlayerOneCard.Add(_AllPLayerCardPile[0]); // Add Card to Player.
                        _AllPLayerCardPile.Remove(_AllPLayerCardPile[0]); // Remove Card from Pile.
                    }
                    else
                    {
                        Debug.Log("Epic");
                        _EpicEvent();
                        _isEpicEvent = true;
                    }

                }
                else
                {

                    if (!_AllPLayerCardPile[0]._isEpidemics)
                    {
                        _Report(_AllPLayerCardPile[0], " has been picked.");
                        PlayerScript.Instance._PlayerTwoCard.Add(_AllPLayerCardPile[0]);
                        _AllPLayerCardPile.Remove(_AllPLayerCardPile[0]);
                    }
                    else
                    {
                        Debug.Log("Epic");
                        _EpicEvent();
                        _isEpicEvent = true;
                    }

                }

                PlayerScript.Instance._PlayState = 2;
                PlayerScript.Instance._CheckCardOnHand();

                _CityCardCount.text = _AllPLayerCardPile.Count + "";
                _isEpicEvent = false;
            }
            else
            {
                GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
                GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);
            }


        }
        else
        {
            GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
            GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);
        }


    }

    public GameObject _PanelTutorial05;

    public int _isCuredById;
    public void _EpicEvent()
    {


        if (GetComponent<TutorialScript>()._isOnTutorial)
        {
            _Plus++; // Add infection rate.
            if (GetComponent<TutorialScript>()._Step != 24)
            {
                //_Report (GetComponent<TutorialScript> ()._EpidemicsNode, "Epidemic");
                foreach (CardManager._Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._EpidemicsNode)
                    {
                        _Report(x, "Epidemic");
                        _isCuredById = -1;
                        break;
                    }
                }

                foreach (_Card i in _AllPLayerCardPile)
                {
                    if (i._Node = GetComponent<TutorialScript>()._EpidemicsNode)
                    {
                        //_AllPLayerCardPile.Remove (i);
                        break;
                    }
                }

            }
            else
            {
                //_Report (GetComponent<TutorialScript> ()._EpidemicsNode02, "Epidemic");
                foreach (CardManager._Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<TutorialScript>()._EpidemicsNode02)
                    {
                        _Report(x, "is saved. The Red disease is cured.");
                        _isCuredById = 1;
                        break;
                    }
                }

                foreach (_Card i in _AllPLayerCardPile)
                {
                    if (i._Node = GetComponent<TutorialScript>()._EpidemicsNode02)
                    {
                        //_AllPLayerCardPile.Remove (i);
                        break;
                    }
                }
            }


        }
        else if (GetComponent<Tutorial_Section02>()._Section02)
        {
            _Plus++; // Add infection rate.
            if (GetComponent<Tutorial_Section02>()._Step == 14)
            {

                foreach (CardManager._Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[14])
                    {
                        _Report(x, "Epidemic");
                        _isCuredById = -1;
                        break;
                    }
                }

                foreach (_Card i in _AllPLayerCardPile)
                {
                    if (i._Node = GetComponent<Tutorial_Section02>()._Node[14])
                    {
                        _AllPLayerCardPile.Remove(i);
                        break;
                    }
                }
            }
            else
            {
                foreach (CardManager._Card x in _InfectionPile)
                {
                    if (x._Node == GetComponent<Tutorial_Section02>()._Node[12])
                    {
                        _Report(x, "is saved. The Red disease is cured.");
                        _isCuredById = 1;
                        break;
                    }
                }

                foreach (_Card i in _AllPLayerCardPile)
                {
                    if (i._Node = GetComponent<Tutorial_Section02>()._Node[12])
                    {
                        _AllPLayerCardPile.Remove(i);
                        break;
                    }
                }
            }
        }
        else
        {
            _Plus++; // Add infection rate.

            if (_InfectionPile[_InfectionPile.Count - 1]._Node.GetComponent<NodeScript>()._Type == 1)
            {
                if (!PlayerScript.Instance._isCureType[1])
                {
                    _Report(_InfectionPile[_InfectionPile.Count - 1], "Epidemic");
                    _isCuredById = -1;
                    //_InfectText.text = _InfectionPile [_InfectionPile.Count - 1]._Node.GetComponent<NodeScript>().name + " has been infected.";
                }
                else
                {
                    _Report(_InfectionPile[_InfectionPile.Count - 1], "is saved. The Red disease is cured.");
                    _isCuredById = 1;
                    //_InfectText.text = _InfectionPile [_InfectionPile.Count - 1]._Node.GetComponent<NodeScript>().name+" is save. The Red disease is cured.";
                }
            }
            else
            {
                if (!PlayerScript.Instance._isCureType[0])
                {
                    _Report(_InfectionPile[_InfectionPile.Count - 1], "Epidemic");
                    _isCuredById = -1;
                    //_InfectText.text = _InfectionPile [_InfectionPile.Count - 1]._Node.GetComponent<NodeScript>().name + " has been infected.";
                }
                else
                {
                    _Report(_InfectionPile[_InfectionPile.Count - 1], "is saved. The Blue disease is cured.");
                    _isCuredById = 0;
                    //_InfectText.text = _InfectionPile [_InfectionPile.Count - 1]._Node.GetComponent<NodeScript>().name + " is save. The Blue disease is cured.";
                }
            }


        }

        if (_Plus == 1)
        {
            _CardPickRate = 2; // 1
        }
        else if (_Plus >= 2 && _Plus < 5)
        { // 2-4
            _CardPickRate = 3;
        }
        else
        {
            _CardPickRate = 4; // 5
        }
    }

    public void _EpicProcess()
    {

        if (!GetComponent<TutorialScript>()._isOnTutorial && !GetComponent<Tutorial_Section02>()._Section02)
        {
            if (_isCuredById == -1)
            {
                _InfectionPile[_InfectionPile.Count - 1]._Node.GetComponent<NodeScript>()._LevelUp(3, _InfectionPile[_InfectionPile.Count - 1]._Node.GetComponent<NodeScript>()._Type);
                // PlayerScript.Instance._DeInfection(_InfectionPile[_InfectionPile.Count - 1]._Node.GetComponent<NodeScript>()._Type);
                // PlayerScript.Instance._DeInfection(_InfectionPile[_InfectionPile.Count - 1]._Node.GetComponent<NodeScript>()._Type);

                // Pick Bottom card from Infection pile and add 3 cude.

            }

            _Discardplie.Add(_InfectionPile[_InfectionPile.Count - 1]); // Add Bottom Card to Infection Discard Pile.
            _InfectionPile.Remove(_InfectionPile[_InfectionPile.Count - 1]); // Remove Bottom Card from Infection Pile.


            _PLayerDiscardplie.Add(_AllPLayerCardPile[0]); // Add Epidemics card to Player Discard Pile.
            _AllPLayerCardPile.Remove(_AllPLayerCardPile[0]); // Remove Epidemics card from City Pile.



            // Shuffer DiscardPile
            int _Count = _Discardplie.Count;
            foreach (_Card Z in _Discardplie)
            {
                _TempCardStore_01.Add(Z);
            }

            _Discardplie.Clear();

            for (int c = 0; c < _Count; c++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _Discardplie.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            // Put Discard Pile on Top of Infection Pile

            foreach (_Card v in _InfectionPile)
            {
                _TempCardStore_01.Add(v);
            }

            _InfectionPile.Clear();

            foreach (_Card b in _Discardplie)
            {
                _InfectionPile.Add(b);
            }

            foreach (_Card n in _TempCardStore_01)
            {
                _InfectionPile.Add(n);
            }

            _TempCardStore_01.Clear();
            _Discardplie.Clear();

            //_CityCardCount.text = "have " + _AllPLayerCardPile.Count + " cards lefts.";
            _CityCardOnReport.gameObject.SetActive(true);
            //_CityCardOnReport.text = "Player deck have " + _AllPLayerCardPile.Count + " cards lefts.";
            _CityCardCount.text = _AllPLayerCardPile.Count + "";
            _CityCardOnReport.text = _AllPLayerCardPile.Count + "";
        }
        else if (GetComponent<Tutorial_Section02>()._Section02)
        {
            if (_isCuredById == -1)
            {
                if (GetComponent<Tutorial_Section02>()._Step == 14)
                {
                    GetComponent<Tutorial_Section02>()._Node[14].GetComponent<NodeScript>()._LevelUp(3, GetComponent<Tutorial_Section02>()._Node[14].GetComponent<NodeScript>()._Type);

                }
                else
                {
                    GetComponent<Tutorial_Section02>()._Node[12].GetComponent<NodeScript>()._LevelUp(3, GetComponent<Tutorial_Section02>()._Node[12].GetComponent<NodeScript>()._Type);

                }
            }



            // Shuffer DiscardPile
            int _Count = _Discardplie.Count;
            foreach (_Card Z in _Discardplie)
            {
                _TempCardStore_01.Add(Z);
            }

            _Discardplie.Clear();

            for (int c = 0; c < _Count; c++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _Discardplie.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            // Put Discard Pile on Top of Infection Pile

            foreach (_Card v in _InfectionPile)
            {
                _TempCardStore_01.Add(v);
            }

            _InfectionPile.Clear();

            foreach (_Card b in _Discardplie)
            {
                _InfectionPile.Add(b);
            }

            foreach (_Card n in _TempCardStore_01)
            {
                _InfectionPile.Add(n);
            }

            _TempCardStore_01.Clear();
            _Discardplie.Clear();

            //_CityCardCount.text = "have " + _AllPLayerCardPile.Count + " cards lefts.";

            _CityCardOnReport.gameObject.SetActive(true);
            _CityCardCount.text = _AllPLayerCardPile.Count + "";
            _CityCardOnReport.text = _AllPLayerCardPile.Count + "";
            //_CityCardOnReport.text = "Player deck have " + _AllPLayerCardPile.Count + " cards lefts.";
        }
        else if (GetComponent<TutorialScript>()._isOnTutorial)
        {
            if (_isCuredById == -1)
            {
                if (GetComponent<TutorialScript>()._Step != 24)
                {
                    GetComponent<TutorialScript>()._EpidemicsNode.GetComponent<NodeScript>()._LevelUp(3, GetComponent<TutorialScript>()._EpidemicsNode.GetComponent<NodeScript>()._Type);

                }
                else
                {
                    GetComponent<TutorialScript>()._EpidemicsNode02.GetComponent<NodeScript>()._LevelUp(3, GetComponent<TutorialScript>()._EpidemicsNode02.GetComponent<NodeScript>()._Type);

                }
            }

            // Shuffer DiscardPile
            int _Count = _Discardplie.Count;
            foreach (_Card Z in _Discardplie)
            {
                _TempCardStore_01.Add(Z);
            }

            _Discardplie.Clear();

            for (int c = 0; c < _Count; c++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _Discardplie.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            // Put Discard Pile on Top of Infection Pile

            foreach (_Card v in _InfectionPile)
            {
                _TempCardStore_01.Add(v);
            }

            _InfectionPile.Clear();

            foreach (_Card b in _Discardplie)
            {
                _InfectionPile.Add(b);
            }

            foreach (_Card n in _TempCardStore_01)
            {
                _InfectionPile.Add(n);
            }

            _TempCardStore_01.Clear();
            _Discardplie.Clear();

            //_CityCardCount.text = "have " + _AllPLayerCardPile.Count + " cards lefts.";
            _CityCardOnReport.gameObject.SetActive(true);
            _CityCardCount.text = _AllPLayerCardPile.Count + "";
            _CityCardOnReport.text = _AllPLayerCardPile.Count + "";
        }
    }


    // [Header("InfectionCard")]
    public List<_Card> _TempCardStore_01 = new List<_Card>();
    public List<_Card> _TempCardStore_02 = new List<_Card>();

    public List<_Card> _EpidamicDeck = new List<_Card>();

    public void _StartGameSetup()
    {

        if (GetComponent<TutorialScript>()._isOnTutorial)
        {
            foreach (_Card x in _AllCardStore)
            {
                if (x._isEpidemics)
                {
                    _EpidamicDeck.Add(x);
                }
                else
                {
                    _AllPLayerCardPile.Add(x);
                    _InfectionPile.Add(x);
                    x._Node.GetComponent<NodeScript>()._NameUpdate(x._CardName);
                }
            }



            // Give Player Card

            foreach (_Card x in _AllPLayerCardPile)
            { // Node 05
                if (x._Node == GetComponent<TutorialScript>()._PlayerA_StarterNode[0])
                {
                    //  _Report(x, "Have been pick.");
                    PlayerScript.Instance._PlayerOneCard.Add(x);
                    _AllPLayerCardPile.Remove(x);
                    break;
                }
            }

            foreach (_Card x in _AllPLayerCardPile)
            { // Node 24
                if (x._Node == GetComponent<TutorialScript>()._PlayerA_StarterNode[1])
                {
                    // _Report(x, "Have been pick.");
                    PlayerScript.Instance._PlayerOneCard.Add(x);
                    _AllPLayerCardPile.Remove(x);
                    break;
                }
            }

            foreach (_Card x in _AllPLayerCardPile)
            { // Node 14
                if (x._Node == GetComponent<TutorialScript>()._PlayerB_StarterNode[0])
                {
                    //_Report(x, "Have been pick.");
                    PlayerScript.Instance._PlayerTwoCard.Add(x);
                    _AllPLayerCardPile.Remove(x);
                    break;
                }
            }

            foreach (_Card x in _AllPLayerCardPile)
            { // Node 16
                if (x._Node == GetComponent<TutorialScript>()._PlayerB_StarterNode[1])
                {
                    //_Report(x, "Have been pick.");
                    PlayerScript.Instance._PlayerTwoCard.Add(x);
                    _AllPLayerCardPile.Remove(x);
                    break;
                }
            }


            // Random All Card

            for (int i = 0; i < _AllPLayerCardPile.Count; i++)
            {
                _TempCardStore_01.Add(_AllPLayerCardPile[i]);
            }

            _AllPLayerCardPile.Clear();

            int _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _AllPLayerCardPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            for (int i = 0; i < _AllPLayerCardPile.Count; i++)
            {
                _TempCardStore_01.Add(_AllPLayerCardPile[i]);
            }

            _AllPLayerCardPile.Clear();

            _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _AllPLayerCardPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            _TempCardStore_01.Clear();

            // Random and Add Epidamic

            for (int i = 0; i < _AllPLayerCardPile.Count; i++)
            {
                if (i < _AllPLayerCardPile.Count / 2)
                {
                    _TempCardStore_01.Add(_AllPLayerCardPile[i]);
                }
                else
                {
                    _TempCardStore_02.Add(_AllPLayerCardPile[i]);
                }

            }

            _AllPLayerCardPile.Clear();

            _TempCardStore_01.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardStore_01.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _AllPLayerCardPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            _TempCardStore_02.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardStore_02.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardPile = _TempCardStore_02.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_02.Count);

                _AllPLayerCardPile.Add(_TempCardStore_02[_random]);
                _TempCardStore_02.RemoveAt(_random);
            }

            //_SetupForStarter ();

            _TempCardStore_01.Clear();
            _TempCardStore_02.Clear();
            // Shuffe Infection Piles

            for (int i = 0; i < _InfectionPile.Count; i++)
            {
                if (i < _InfectionPile.Count / 2)
                {
                    _TempCardStore_01.Add(_InfectionPile[i]);
                }
                else
                {
                    _TempCardStore_02.Add(_InfectionPile[i]);
                }

            }

            _InfectionPile.Clear();

            _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _InfectionPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            _TempCardPile = _TempCardStore_02.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_02.Count);

                _InfectionPile.Add(_TempCardStore_02[_random]);
                _TempCardStore_02.RemoveAt(_random);
            }


            PlayerScript.Instance._StartGame();
            PlayerScript.Instance._CheckCard();

            _CityCardCount.text = _AllPLayerCardPile.Count + "";

        }
        else if (GetComponent<Tutorial_Section02>()._Section02)
        {

            foreach (_Card x in _AllCardStore)
            {
                if (x._isEpidemics)
                {
                    _EpidamicDeck.Add(x);
                }
                else
                {
                    _AllPLayerCardPile.Add(x);
                    _InfectionPile.Add(x);
                    x._Node.GetComponent<NodeScript>()._NameUpdate(x._CardName);
                }
            }



            // Give Player Card

            foreach (_Card x in _AllPLayerCardPile)
            { // Node 05
                if (x._Node == GetComponent<Tutorial_Section02>()._Node[3])
                {
                    //  _Report(x, "Have been pick.");
                    PlayerScript.Instance._PlayerOneCard.Add(x);
                    _AllPLayerCardPile.Remove(x);
                    break;
                }
            }

            foreach (_Card x in _AllPLayerCardPile)
            { // Node 24
                if (x._Node == GetComponent<Tutorial_Section02>()._Node[9])
                {
                    // _Report(x, "Have been pick.");
                    PlayerScript.Instance._PlayerOneCard.Add(x);
                    _AllPLayerCardPile.Remove(x);
                    break;
                }
            }

            foreach (_Card x in _AllPLayerCardPile)
            { // Node 14
                if (x._Node == GetComponent<Tutorial_Section02>()._Node[16])
                {
                    //_Report(x, "Have been pick.");
                    PlayerScript.Instance._PlayerTwoCard.Add(x);
                    _AllPLayerCardPile.Remove(x);
                    break;
                }
            }

            foreach (_Card x in _AllPLayerCardPile)
            { // Node 16
                if (x._Node == GetComponent<Tutorial_Section02>()._Node[2])
                {
                    //_Report(x, "Have been pick.");
                    PlayerScript.Instance._PlayerTwoCard.Add(x);
                    _AllPLayerCardPile.Remove(x);
                    break;
                }
            }


            // Random All Card

            for (int i = 0; i < _AllPLayerCardPile.Count; i++)
            {
                _TempCardStore_01.Add(_AllPLayerCardPile[i]);
            }

            _AllPLayerCardPile.Clear();

            int _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _AllPLayerCardPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            for (int i = 0; i < _AllPLayerCardPile.Count; i++)
            {
                _TempCardStore_01.Add(_AllPLayerCardPile[i]);
            }

            _AllPLayerCardPile.Clear();

            _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _AllPLayerCardPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            _TempCardStore_01.Clear();

            // Random and Add Epidamic

            for (int i = 0; i < _AllPLayerCardPile.Count; i++)
            {
                if (i < _AllPLayerCardPile.Count / 2)
                {
                    _TempCardStore_01.Add(_AllPLayerCardPile[i]);
                }
                else
                {
                    _TempCardStore_02.Add(_AllPLayerCardPile[i]);
                }

            }

            _AllPLayerCardPile.Clear();

            _TempCardStore_01.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardStore_01.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _AllPLayerCardPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            _TempCardStore_02.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardStore_02.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardPile = _TempCardStore_02.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_02.Count);

                _AllPLayerCardPile.Add(_TempCardStore_02[_random]);
                _TempCardStore_02.RemoveAt(_random);
            }

            _TempCardStore_01.Clear();
            _TempCardStore_02.Clear();
            // Shuffe Infection Piles

            for (int i = 0; i < _InfectionPile.Count; i++)
            {
                if (i < _InfectionPile.Count / 2)
                {
                    _TempCardStore_01.Add(_InfectionPile[i]);
                }
                else
                {
                    _TempCardStore_02.Add(_InfectionPile[i]);
                }

            }

            _InfectionPile.Clear();

            _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _InfectionPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            _TempCardPile = _TempCardStore_02.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_02.Count);

                _InfectionPile.Add(_TempCardStore_02[_random]);
                _TempCardStore_02.RemoveAt(_random);
            }


            PlayerScript.Instance._StartGame();
            PlayerScript.Instance._CheckCard();

            _CityCardCount.text = _AllPLayerCardPile.Count + "";

        }
        else
        {
            foreach (_Card x in _AllCardStore)
            {
                if (x._isEpidemics)
                {
                    _EpidamicDeck.Add(x);
                }
                else
                {
                    _AllPLayerCardPile.Add(x);
                    _InfectionPile.Add(x);
                    x._Node.GetComponent<NodeScript>()._NameUpdate(x._CardName);
                }
            }



            // Give Player Card

            for (int i = 0; i < 2; i++)
            {
                _Random = Random.Range(0, _AllPLayerCardPile.Count);
                PlayerScript.Instance._PlayerOneCard.Add(_AllPLayerCardPile[_Random]);
                _AllPLayerCardPile.Remove(_AllPLayerCardPile[_Random]);
            }

            for (int i = 0; i < 2; i++)
            {
                _Random = Random.Range(0, _AllPLayerCardPile.Count);
                PlayerScript.Instance._PlayerTwoCard.Add(_AllPLayerCardPile[_Random]);
                _AllPLayerCardPile.Remove(_AllPLayerCardPile[_Random]);
            }

            // Random All Card

            for (int i = 0; i < _AllPLayerCardPile.Count; i++)
            {
                _TempCardStore_01.Add(_AllPLayerCardPile[i]);
            }

            _AllPLayerCardPile.Clear();

            int _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _AllPLayerCardPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            for (int i = 0; i < _AllPLayerCardPile.Count; i++)
            {
                _TempCardStore_01.Add(_AllPLayerCardPile[i]);
            }

            _AllPLayerCardPile.Clear();

            _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _AllPLayerCardPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            _TempCardStore_01.Clear();

            // Random and Add Epidamic

            for (int i = 0; i < _AllPLayerCardPile.Count; i++)
            {
                if (i < _AllPLayerCardPile.Count / 2)
                {
                    _TempCardStore_01.Add(_AllPLayerCardPile[i]);
                }
                else
                {
                    _TempCardStore_02.Add(_AllPLayerCardPile[i]);
                }

            }

            _AllPLayerCardPile.Clear();

            _TempCardStore_01.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardStore_01.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _AllPLayerCardPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            _TempCardStore_02.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardStore_02.Add(_EpidamicDeck[0]);
            _EpidamicDeck.RemoveAt(0);
            _TempCardPile = _TempCardStore_02.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_02.Count);

                _AllPLayerCardPile.Add(_TempCardStore_02[_random]);
                _TempCardStore_02.RemoveAt(_random);
            }


            /// Setup Infection City

            for (int i = 0; i < 3; i++)
            {
                _Random = Random.Range(0, _InfectionPile.Count);
                _InfectionPile[_Random]._Node.GetComponent<NodeScript>()._InfectionForSetup(2, _InfectionPile[_Random]._Node.GetComponent<NodeScript>()._Type);

                PlayerScript.Instance._DeInfection(_InfectionPile[_Random]._Node.GetComponent<NodeScript>()._Type, 2);
                // PlayerScript.Instance._DeInfection(_InfectionPile[_Random]._Node.GetComponent<NodeScript>()._Type,1);

                _Report(_InfectionPile[_Random], " - has 2 orders");
                _Discardplie.Add(_InfectionPile[_Random]);
                _InfectionPile.RemoveAt(_Random);

            }

            for (int i = 0; i < 3; i++)
            {
                _Random = Random.Range(0, _InfectionPile.Count);
                _InfectionPile[_Random]._Node.GetComponent<NodeScript>()._InfectionForSetup(1, _InfectionPile[_Random]._Node.GetComponent<NodeScript>()._Type);

                PlayerScript.Instance._DeInfection(_InfectionPile[_Random]._Node.GetComponent<NodeScript>()._Type, 1);


                _Report(_InfectionPile[_Random], " - has 1 order");
                _Discardplie.Add(_InfectionPile[_Random]);
                _InfectionPile.RemoveAt(_Random);

            }

            _TempCardStore_01.Clear();
            _TempCardStore_02.Clear();
            // Shuffe Infection Piles

            for (int i = 0; i < _InfectionPile.Count; i++)
            {
                if (i < _InfectionPile.Count / 2)
                {
                    _TempCardStore_01.Add(_InfectionPile[i]);
                }
                else
                {
                    _TempCardStore_02.Add(_InfectionPile[i]);
                }

            }

            _InfectionPile.Clear();

            _TempCardPile = _TempCardStore_01.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_01.Count);

                _InfectionPile.Add(_TempCardStore_01[_random]);
                _TempCardStore_01.RemoveAt(_random);
            }

            _TempCardPile = _TempCardStore_02.Count;
            for (int i = 0; i < _TempCardPile; i++)
            {
                int _random = Random.Range(0, _TempCardStore_02.Count);

                _InfectionPile.Add(_TempCardStore_02[_random]);
                _TempCardStore_02.RemoveAt(_random);
            }


            PlayerScript.Instance._StartGame();
            PlayerScript.Instance._CheckCard();

            _CityCardCount.text = _AllPLayerCardPile.Count + "";


        }

        GetComponent<LogFilesScript>()._isStartCounting = true;


    }

    public bool _IsStartCount;

    public void _SetupForStarter()
    {

        if (GetComponent<TutorialScript>()._isOnTutorial)
        {

            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<TutorialScript>()._Node01)
                {
                    //	x._Node.GetComponent<NodeScript> ()._InfectionForSetup (1, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 1);
                    _Report(x, " - has 1 order");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);

                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 1;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);

                    break;
                }
            }


            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<TutorialScript>()._Node17)
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (1, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 1);
                    _Report(x, " - has 1 order");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);

                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 1;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }

            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<TutorialScript>()._Node10)
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (2, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 2);
                    _Report(x, " - has 1 order");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);
                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 1;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }

            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<TutorialScript>()._Node21)
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (1, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 1);
                    _Report(x, " - has 2 orders");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);

                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 2;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }



            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<TutorialScript>()._Node14)
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (2, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 2);
                    _Report(x, " - has 2 orders");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);
                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 2;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }

            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<TutorialScript>()._Node18)
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (2, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 2);
                    _Report(x, " - has 2 orders");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);
                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 2;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }


        }
        else
        {

            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<Tutorial_Section02>()._Node[3])
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (1, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 1);
                    _Report(x, " - has 1 order");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);
                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 1;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }

            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<Tutorial_Section02>()._Node[15])
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (1, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 1);
                    _Report(x, " - has 1 order");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);
                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 1;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }

            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<Tutorial_Section02>()._Node[20])
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (1, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 1);
                    _Report(x, " - has 1 order");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);
                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 1;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }

            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<Tutorial_Section02>()._Node[1])
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (2, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 2);
                    _Report(x, " - has 2 orders");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);
                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 2;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }

            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<Tutorial_Section02>()._Node[21])
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (2, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 2);
                    _Report(x, " - has 2 orders");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);
                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 2;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }

            foreach (_Card x in _InfectionPile)
            {
                if (x._Node == GetComponent<Tutorial_Section02>()._Node[23])
                {
                    //x._Node.GetComponent<NodeScript> ()._InfectionForSetup (2, x._Node.GetComponent<NodeScript> ()._Type);
                    //PlayerScript.Instance._DeInfection (x._Node.GetComponent<NodeScript> ()._Type, 2);
                    _Report(x, " - has 2 orders");
                    //_Discardplie.Add (x);
                    //_InfectionPile.Remove (x);
                    InfectionEvent _Event = new InfectionEvent();
                    _Event._InfectionLevel = 2;
                    _Event._Node = x._Node;
                    _InfectionStore.Add(_Event);
                    break;
                }
            }
        }

        _InfectionBanner();
        _NodeManger.GetComponent<NodeManager>()._Reset();
    }

    public GameObject[] _OutBreakCube;
    public GameObject[] _InfectionCube;
    public GameObject _NodeManger;
    public void _OutBreakAndInfectionTap()
    {
        for (int i = 0; i < _OutBreakCube.Length; i++)
        {
            if (i < _NodeManger.GetComponent<NodeManager>()._OutBreak)
            {
                _OutBreakCube[i].GetComponent<Image>().color = Color.red;
            }
            else
            {
                _OutBreakCube[i].GetComponent<Image>().color = Color.white;
            }
        }

        for (int i = 0; i < _InfectionCube.Length; i++)
        {
            if (i < _Plus)
            {
                _InfectionCube[i].GetComponent<Image>().color = Color.red;
            }
            else
            {
                _InfectionCube[i].GetComponent<Image>().color = Color.white;
            }
        }

        if (GetComponent<TutorialScript>()._Step == 12)
        {
            _PanelTutorial05.SetActive(true);
        }
    }


    public GameObject _Prefab;
    public GameObject _Prefab_Deck;
    public GameObject _Contant;
    public void _CreateCityDiscard()
    {
        foreach (Transform child in _Contant.transform)
        {
            Destroy(child.gameObject);
        }


        foreach (CardManager._Card x in _PLayerDiscardplie)
        {
            GameObject i = (GameObject)Instantiate(_Prefab_Deck, _Contant.transform);
            i.GetComponent<Image>().color = x._Node.GetComponent<NodeScript>()._NodeColor[x._Node.GetComponent<NodeScript>()._Type];
            i.transform.GetChild(0).gameObject.GetComponent<Text>().text = x._CardName;
        }

    }

    public GameObject _Contant_Infection;
    public void _CreateInfectionDiscard()
    {

        foreach (Transform child in _Contant_Infection.transform)
        {
            Destroy(child.gameObject);
        }

        foreach (CardManager._Card x in _Discardplie)
        {
            GameObject i = (GameObject)Instantiate(_Prefab_Deck, _Contant_Infection.transform);
            i.GetComponent<Image>().color = x._Node.GetComponent<NodeScript>()._NodeColor[x._Node.GetComponent<NodeScript>()._Type];
            i.transform.GetChild(0).gameObject.GetComponent<Text>().text = x._CardName;
        }

    }

    public GameObject _ReportPopUp;
    public GameObject _Contant_Report;

    public GameObject _RightCardPanel;
    public GameObject _OpenTapPanel;

    public GameObject _PanelTutorial02;
    public GameObject _PanelTutorial03;
    public GameObject _PanelTutorial06;
    public GameObject _PanelTutorial07;

    public Text _ReportTxt;

    bool _isStart;

    public GameObject _BtmForToturial;
    bool _isFromShare;
    public void _CloseReport()
    {
        foreach (Transform child in _Contant_Report.transform)
        {
            Destroy(child.gameObject);
        }

        if (!PlayerScript.Instance._isFromShares)
        {
            if (_isStart && !_isEpicEvent && !GetComponent<Tutorial_Section02>()._Section02 && !GetComponent<TutorialScript>()._isOnTutorial)
            {
                if (PlayerScript.Instance._State == 1)
                {
                    if (PlayerScript.Instance._PlayerOneCard.Count <= 5)
                    {
                        GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
                        GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);
                    }
                }
                else
                {
                    if (PlayerScript.Instance._PlayerTwoCard.Count <= 5)
                    {
                        GetComponent<PlayerScript>()._PhasePanel.SetActive(true);
                        GetComponent<PlayerScript>()._PhaseSubPanel[3].SetActive(true);
                    }
                }
            }
        }


        //GetComponent<PlayerScript> ()._PhasePanel.SetActive (true);
        //GetComponent<PlayerScript> ()._PhaseSubPanel [3].SetActive (true);

        _ReportPopUp.SetActive(false);

        if (GetComponent<TutorialScript>()._Step == 3)
        {
            _RightCardPanel.SetActive(true);
            _OpenTapPanel.SetActive(false);
            GetComponent<PlayerScript>()._CardOnHand();
            GetComponent<TutorialScript>()._ExtraPopUp[6].SetActive(true);
            _BtmForToturial.SetActive(true);

        }

        if (GetComponent<TutorialScript>()._Step == 5)
        {
            _RightCardPanel.SetActive(true);
            _OpenTapPanel.SetActive(false);
            GetComponent<PlayerScript>()._CardOnHand();
            _PanelTutorial02.SetActive(true);
            GetComponent<TutorialScript>()._StepUp();
            _BtmForToturial.SetActive(true);
        }

        if (GetComponent<TutorialScript>()._Step == 27)
        {
            GetComponent<TutorialScript>()._ShowPopUp();
        }

        if (GetComponent<Tutorial_Section02>()._Section02)
        {
            if (GetComponent<Tutorial_Section02>()._Step == 0)
            {
                //GetComponent<Tutorial_Section02> ()._Tu [0].SetActive (true);
            }
        }

        if (GetComponent<TutorialScript>()._isOnTutorial)
        {
            if (GetComponent<TutorialScript>()._Step == 0)
            {
                _PanelTutorial.SetActive(true);
            }
        }

        _isStart = true;

        PlayerScript.Instance._isFromShares = false;

    }



    public void _ShowCardOnHand()
    {
        if (GetComponent<TutorialScript>()._Step == 7)
        {
            _RightCardPanel.SetActive(true);
            _OpenTapPanel.SetActive(false);
            GetComponent<PlayerScript>()._CardOnHand();
            _PanelTutorial03.SetActive(true);
        }

        if (GetComponent<TutorialScript>()._Step == 11)
        {
            _RightCardPanel.SetActive(true);
            _OpenTapPanel.SetActive(false);
            GetComponent<PlayerScript>()._CardOnHand();
            //_PanelTutorial03.SetActive (true);
        }

        if (GetComponent<TutorialScript>()._Step == 21)
        {
            _RightCardPanel.SetActive(true);
            _OpenTapPanel.SetActive(false);
            GetComponent<PlayerScript>()._CardOnHand();
            //_PanelTutorial03.SetActive (true);
        }

        if (GetComponent<Tutorial_Section02>()._Section02)
        {
            if (GetComponent<Tutorial_Section02>()._Step == 12)
            {
                _RightCardPanel.SetActive(true);
                _OpenTapPanel.SetActive(false);
                GetComponent<PlayerScript>()._CardOnHand();
            }
        }
    }

    public GameObject _EpidemicText;
    public Text _infectionText;

    public GameObject _OutBreakReport;
    public Text _OutBreakText;
    public GameObject _LeftBar;
    public void _Report(_Card i, string z)
    {
        _ReportPopUp.SetActive(true);


        GameObject Node = (GameObject)Instantiate(_Prefab, _Contant_Report.transform);
        Node.GetComponent<Image>().color = i._Node.GetComponent<NodeScript>()._NodeColor[i._Node.GetComponent<NodeScript>()._Type];
        if (z == "Infection")
        {
            int Lv = i._Node.GetComponent<NodeScript>()._Level;
            if (Lv == 0)
            {
                Lv = 1;
            }
            else
            {
                Lv += 1;
            }
            Node.transform.GetChild(0).gameObject.GetComponent<Text>().text = i._CardName + " - " + z + " Lv " + (Lv);
        }
        else if (z == "Infection From Outbreak")
        {
            //_OutBreakReport.SetActive (true);
            //_OutBreakText.text = "Each city connected to " + i._CardName + " Infection level increse by 1.";
            _LeftBar.SetActive(true);
            _OutBreakAndInfectionTap();
            //Node.transform.GetChild(0).gameObject.GetComponent<Text>().text = i._CardName + " - has "+(i._Node.GetComponent<NodeScript>()._Level+1)+" infections" + " From Outbreak";
            Node.transform.GetChild(0).gameObject.GetComponent<Text>().text = "ถนน " + i._CardName + " ได้รับผลกระทบจากการจราจรติดขัดของถนนข้างเคียง";

        }
        else
        {


            Node.transform.GetChild(0).gameObject.GetComponent<Text>().text = i._CardName + z;

            if (z == "Epidemic")
            {
                Node.transform.GetChild(0).gameObject.GetComponent<Text>().text = i._CardName + " - Market Expansion";
                _infectionText.text = i._CardName + " ถูกหยิบจากกอง Procument order แสดงว่ามีการส่งสินค้าเพิ่ม\nทำให้เพิ่มระดับ congestion level ในถนน " + i._CardName + " เป็น 3 ระดับ";

                _EpidemicText.SetActive(true);
            }
            else if (z == "is saved. The Red disease is cured.")
            {
                Node.transform.GetChild(0).gameObject.GetComponent<Text>().text = i._CardName + " - Market Expansion";
                _infectionText.text = "พื้นที่สีแดงได้รับการแก้ปัญหาแล้ว\nฉะนั้นถนน " + i._CardName + " จะไม่เกิดปัญหาการจารจรแออัดอีก";

                _EpidemicText.SetActive(true);
            }
            else if (z == "is saved. The Blue disease is cured.")
            {
                Node.transform.GetChild(0).gameObject.GetComponent<Text>().text = i._CardName + " - Market Expansion";
                _infectionText.text = "พื้นที่สีน้ำเงินได้รับการแก้ปัญหาแล้ว\nฉะนั้นถนน " + i._CardName + " จะไม่เกิดปัญหาการจารจรแออัดอีก";
                _EpidemicText.SetActive(true);
            }
        }

    }

    public Text _IncresaeText;
    public void _Report_CityCard(_Card i, string z)
    {

        _ReportPopUp.SetActive(true);


        GameObject Node = (GameObject)Instantiate(_Prefab, _Contant_Report.transform);
        Node.GetComponent<Image>().color = i._Node.GetComponent<NodeScript>()._NodeColor[i._Node.GetComponent<NodeScript>()._Type];
        if (z == "Infection")
        {
            int Lv = i._Node.GetComponent<NodeScript>()._Level;
            if (Lv == 0)
            {
                Lv = 1;
            }
            else
            {
                Lv += 1;
            }
            Node.transform.GetChild(0).gameObject.GetComponent<Text>().text = i._CardName + " - " + z + " Lv " + (Lv);
        }
        else if (z == "Infection From Outbreak")
        {
            //_OutBreakReport.SetActive (true);
            //_OutBreakText.text = "Each city connected to " + i._CardName + " Infection level increse by 1.";
            _LeftBar.SetActive(true);
            _OutBreakAndInfectionTap();
            Node.transform.GetChild(0).gameObject.GetComponent<Text>().text = i._CardName + " - Infection " + " Lv " + (i._Node.GetComponent<NodeScript>()._Level + 1) + " From Outbreak";
        }
        else
        {
            Node.transform.GetChild(0).gameObject.GetComponent<Text>().text = i._CardName + " card " + z;
            if (z == "Epidemic")
            {
                _infectionText.text = i._CardName + " card is drawn from the Infection Deck.\n" + i._CardName + " infected by 3 infections.";

                if (_Plus == 1)
                {
                    //_CardPickRate = 2; // 1
                    _IncresaeText.text = "Infection rate is increase to 2.";
                }
                else if (_Plus >= 2 && _Plus < 5)
                { // 2-4
                  //_CardPickRate = 3;
                    _IncresaeText.text = "Infection rate is increase from 2 to 3.";
                }
                else
                {
                    //_CardPickRate = 4; // 5
                    _IncresaeText.text = "Infection rate is increase from 3 to 4.";
                }

                _EpidemicText.SetActive(true);

            }
        }
    }

    public GameObject _EndGamePopUp;
    public Text _EndGameText;
    public Image _EndGameImage_win;
    public Image _EndGameImage_lose;
    bool _isSave;
    public void _EndGame(bool _isWin, string _Text)
    {
        if (_isWin)
        {
            _EndGameText.text = "Win";
            _EndGameImage_win.enabled = true;
            _EndGameImage_lose.enabled = false;

            GetComponent<LogFilesScript>()._EndGameBy = "Win : " + _Text;
            GetComponent<LogFilesScript>()._GameResult = "Win";
            _EndGamePopUp.SetActive(true);
            SoundManager.instance.StopSound(SoundType.SFX_1);
            SoundManager.instance.PlaySound("Cheering_Big", SoundType.SFX_1);
        }
        else
        {
            _EndGameText.text = "Lose";
            _EndGameImage_lose.enabled = true;
            _EndGameImage_win.enabled = false;
            GetComponent<LogFilesScript>()._EndGameBy = "Lose : " + _Text;
            GetComponent<LogFilesScript>()._GameResult = "Lose";
            _EndGamePopUp.SetActive(true);
            SoundManager.instance.StopSound(SoundType.SFX_1);
            SoundManager.instance.PlaySound("Yelling", SoundType.SFX_1);
        }


        GetComponent<LogFilesScript>()._isStartCounting = false;
        if (!_isSave)
        {
            GetComponent<LogFilesScript>()._CalBeforeWrtie();
            _isSave = true;
        }

    }

    public void _Restart()
    {
        if (!_isSave)
        {
            GetComponent<LogFilesScript>()._GameResult = "Restart";
            GetComponent<LogFilesScript>()._EndGameBy = "Restart";
            GetComponent<LogFilesScript>()._isStartCounting = false;
            GetComponent<LogFilesScript>()._CalBeforeWrtie();
        }
        //Application.LoadLevel (0);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }


    public void _Quit()
    {
        Application.Quit();
    }
}
