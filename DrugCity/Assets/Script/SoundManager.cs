﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public struct Sound
{
    public AudioClip sound;
    public string name;
}

public enum SoundType
{
    BGM = 0,
    SFX_1,
    SFX_2,
    Ambient
}

public class SoundManager : MonoBehaviour
{
    public Object[] audioClips;

    List<Sound> soundClip = new List<Sound>();
    public List<AudioSource> audioSrc = new List<AudioSource>();

    public TMP_Dropdown bgm_dropdown;
    List<Sound> bgmClip = new List<Sound>();


    public static SoundManager instance;

    // Use this for initialization
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }


        audioClips = Resources.LoadAll("Audio", typeof(AudioClip));

        if (bgm_dropdown != null) bgm_dropdown.ClearOptions();

        for (int x = 0; x < audioClips.Length; x++)
        {
            //Debug.Log("?");
            Sound sound = new Sound();
            sound.sound = (AudioClip)audioClips[x];
            sound.name = audioClips[x].name;
            soundClip.Add(sound);
            //Debug.Log("Add sound: " + sound.name);



            if (audioClips[x].name.Contains("bgm"))
            {
                bgmClip.Add(sound);
                if (bgm_dropdown != null) bgm_dropdown.options.Add(new TMP_Dropdown.OptionData(audioClips[x].name));
            }


        }
        foreach (AudioSource item in gameObject.GetComponents<AudioSource>())
        {
            audioSrc.Add(item);
        }

        //PlayBGM(0);

    }

    public void PlayBGM(int index)
    {
        audioSrc[(int)SoundType.BGM].Stop();
        audioSrc[(int)SoundType.BGM].clip = bgmClip[index].sound;
        audioSrc[(int)SoundType.BGM].Play();
    }

    public void PlaySound(string clip, SoundType type)
    {
        StopSound(type);

        AudioClip playClip = null;

        for (int i = 0; i < soundClip.Count; i++)
        {
            if (clip == soundClip[i].name)
            {
                playClip = soundClip[i].sound;
            }
        }
        if (playClip == null)
        {
            Debug.Log("Sound: " + clip + " not found.");
        }


        audioSrc[(int)type].clip = playClip;
        audioSrc[(int)type].Play();
    }
    public void PlaySound(string clip, AudioSource source)
    {

        AudioClip playClip = null;

        for (int i = 0; i < soundClip.Count; i++)
        {
            if (clip == soundClip[i].name)
            {
                playClip = soundClip[i].sound;
            }
        }

        source.clip = playClip;
        source.Play();

    }

    public void setVolume(float volume, SoundType type)
    {

        audioSrc[(int)type].volume = volume;
    }

    public void StopSound(SoundType type)
    {
        audioSrc[(int)type].Stop();
    }

    public void PlaySound(AudioClip clip)
    {
        //audioSrc.PlayOneShot(clip);
    }
}
