﻿using System.Collections;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class TutorialNew : MonoBehaviour
{

    [System.Serializable]
    public class ActiveDetail
    {
        public int StartStep = 0;
        public List<GameObject> activeList;
        public List<GameObject> inactiveList;
    }

    [SerializeField]
    public List<ActiveDetail> activeDetails;

    public static TutorialNew instance;

    public int chapter = 1;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {

        //setDialougeText();
    }

    public List<List<Dictionary<string, string>>> tutorial_texts = new List<List<Dictionary<string, string>>>();

    //public int dialougeIndex = 0;
    //public GameObject dialougeBox;
    //public TextMeshProUGUI dialougeText;

    //public void onNextClick()
    //{
    //    dialougeIndex++;
    //    setDialougeText();
    //}
    //public void setDialougeText()
    //{
    //    var lanstr = TextManager.instance.getLangString();
    //    var result = "";
    //    var textDataList = tutorial_texts[chapter][dialougeIndex];
    //    if (textDataList.TryGetValue(lanstr, out result))
    //        dialougeText.text = result;
    //}

    public void setChapter(int index)
    {
        chapter = index;
    }

    public enum PlayerIndex
    {
        PLAYER_1,
        PLAYER_2
    }
    PlayerIndex playerIndex;

    void setPlayerPos(PlayerIndex playerIndex, GameObject targetNode)
    {

        var targetPos = targetNode.transform.position;
        targetPos = new Vector3(targetPos.x - 0.1f, targetPos.y, targetPos.z + 0.1f);
        switch (playerIndex)
        {
            case PlayerIndex.PLAYER_1:
                UI_ReferenceManager.instance.playerScript._PlayerOne.transform.position = targetPos;
                UI_ReferenceManager.instance.playerScript._CurrectNodePlayerOne = targetNode;
                break;
            case PlayerIndex.PLAYER_2:
                UI_ReferenceManager.instance.playerScript._PlayerTwo.transform.position = targetPos;
                UI_ReferenceManager.instance.playerScript._CurrectNodePlayerTwo = targetNode;
                break;
        }
    }

    void resetCardPool()
    {
        var cardManager = UI_ReferenceManager.instance.cardManager;

        foreach (CardManager._Card x in cardManager._AllCardStore)
        {
            if (x._isEpidemics)
            {
                cardManager._EpidamicDeck.Add(x);
            }
            else
            {
                cardManager._AllPLayerCardPile.Add(x);
                cardManager._InfectionPile.Add(x);
                x._Node.GetComponent<NodeScript>()._NameUpdate(x._CardName);
            }
        }

        PlayerScript.Instance._PlayerOneCard.Clear();
        PlayerScript.Instance._PlayerTwoCard.Clear();
        PlayerScript.Instance._CheckPlayerCard();
    }

    void setPlayerCard(PlayerIndex playerIndex, GameObject targetNode)
    {

        var targetPos = targetNode.transform.position;


        var cardManager = UI_ReferenceManager.instance.cardManager;
        cardManager._ReportTxt.text = "Picked Card";
        cardManager._isEpicEvent = false;
        foreach (CardManager._Card x in cardManager._AllPLayerCardPile)
        {
            if (x._Node == targetNode)
            {
                cardManager._Report(x, " has been picked.");
                switch (playerIndex)
                {
                    case PlayerIndex.PLAYER_1:
                        PlayerScript.Instance._PlayerOneCard.Add(x);
                        break;
                    case PlayerIndex.PLAYER_2:
                        PlayerScript.Instance._PlayerTwoCard.Add(x);
                        break;
                }
                cardManager._AllPLayerCardPile.Remove(x);
                break;
            }
        }
        cardManager._CityCardOnReport.gameObject.SetActive(true);
        cardManager._CityCardCount.text = cardManager._AllPLayerCardPile.Count + "";
        cardManager._CityCardOnReport.text = cardManager._AllPLayerCardPile.Count + "";

        PlayerScript.Instance._CheckPlayerCard();

        PlayerScript.Instance._CardObject[0].GetComponent<Button>().interactable = true;
        PlayerScript.Instance._CardObject[1].GetComponent<Button>().interactable = true;
        PlayerScript.Instance._CardObject[2].GetComponent<Button>().interactable = true;
        PlayerScript.Instance._CardObject[3].GetComponent<Button>().interactable = true;
        PlayerScript.Instance._CardObject[4].GetComponent<Button>().interactable = true;

    }

    public void StartTutorial()
    {

        foreach (var item in activeDetails[chapter].inactiveList)
        {
            item.SetActive(false);
        }
        foreach (var item in activeDetails[chapter].activeList)
        {
            item.SetActive(true);
        }
        UI_ReferenceManager.instance.TutorialPanel.SetActive(true);
        UI_ReferenceManager.instance.tutorialScript._isTutorial();
        UI_ReferenceManager.instance.tutorialScript._Step = activeDetails[chapter].StartStep;

        switch (chapter)
        {
            case 0:
                //player pos

                //player card

                //node status

                break;
            case 1:

                //BgManager.instance.setBG();

                //player pos
                setPlayerPos(PlayerIndex.PLAYER_1, UI_ReferenceManager.instance.tutorialScript._Node11);
                setPlayerPos(PlayerIndex.PLAYER_2, UI_ReferenceManager.instance.tutorialScript._Node17);

                //player card
                resetCardPool();
                setPlayerCard(PlayerIndex.PLAYER_1, UI_ReferenceManager.instance.tutorialScript._Node22);
                setPlayerCard(PlayerIndex.PLAYER_1, UI_ReferenceManager.instance.tutorialScript._Node19);
                setPlayerCard(PlayerIndex.PLAYER_1, UI_ReferenceManager.instance.tutorialScript._Node21);

                setPlayerCard(PlayerIndex.PLAYER_2, UI_ReferenceManager.instance.tutorialScript._Node08);
                setPlayerCard(PlayerIndex.PLAYER_2, UI_ReferenceManager.instance.tutorialScript._Node07);

                //node status
                UI_ReferenceManager.instance.cardManager._ResetNode();
                UI_ReferenceManager.instance.tutorialScript._Node08.GetComponent<NodeScript>()._LevelUp(1);
                UI_ReferenceManager.instance.tutorialScript._Node09.GetComponent<NodeScript>()._LevelUp(1);
                UI_ReferenceManager.instance.tutorialScript._Node10.GetComponent<NodeScript>()._LevelUp(1);
                UI_ReferenceManager.instance.tutorialScript._Node17.GetComponent<NodeScript>()._LevelUp(2);
                UI_ReferenceManager.instance.tutorialScript._Node18.GetComponent<NodeScript>()._LevelUp(2);
                UI_ReferenceManager.instance.tutorialScript._Node19.GetComponent<NodeScript>()._LevelUp(1);
                UI_ReferenceManager.instance.tutorialScript._Node21.GetComponent<NodeScript>()._LevelUp(3);
                UI_ReferenceManager.instance.tutorialScript._Node23.GetComponent<NodeScript>()._LevelUp(3);
                UI_ReferenceManager.instance.tutorialScript._Node24.GetComponent<NodeScript>()._LevelUp(1);

                break;
            case 2:
                BgManager.instance.setBG(1);
                //player pos
                setPlayerPos(PlayerIndex.PLAYER_1, UI_ReferenceManager.instance.tutorialScript._Node19);
                setPlayerPos(PlayerIndex.PLAYER_2, UI_ReferenceManager.instance.tutorialScript._Node17);

                //player card
                resetCardPool();
                setPlayerCard(PlayerIndex.PLAYER_1, UI_ReferenceManager.instance.tutorialScript._Node22);

                setPlayerCard(PlayerIndex.PLAYER_2, UI_ReferenceManager.instance.tutorialScript._Node08);
                setPlayerCard(PlayerIndex.PLAYER_2, UI_ReferenceManager.instance.tutorialScript._Node07);

                //node status
                UI_ReferenceManager.instance.cardManager._ResetNode();
                UI_ReferenceManager.instance.tutorialScript._Node08.GetComponent<NodeScript>()._LevelUp(1);
                UI_ReferenceManager.instance.tutorialScript._Node09.GetComponent<NodeScript>()._LevelUp(1);
                UI_ReferenceManager.instance.tutorialScript._Node10.GetComponent<NodeScript>()._LevelUp(2);

                UI_ReferenceManager.instance.tutorialScript._Node17.GetComponent<NodeScript>()._LevelUp(2);
                UI_ReferenceManager.instance.tutorialScript._Node18.GetComponent<NodeScript>()._LevelUp(2);
                UI_ReferenceManager.instance.tutorialScript._Node19.GetComponent<NodeScript>()._LevelUp(1);

                UI_ReferenceManager.instance.tutorialScript._Node21.GetComponent<NodeScript>()._LevelUp(1);
                UI_ReferenceManager.instance.tutorialScript._Node23.GetComponent<NodeScript>()._LevelUp(3);
                UI_ReferenceManager.instance.tutorialScript._Node24.GetComponent<NodeScript>()._LevelUp(1);


                UI_ReferenceManager.instance.tutorialScript._Node19.GetComponent<NodeScript>()._Build();
                //PlayerScript.Instance._isCureType[1] = true;
                PlayerScript.Instance._State = 1;
                PlayerScript.Instance._ChangeImage();

                break;
        }

    }
    public void ResetTutorial()
    {

        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        UI_ReferenceManager.instance.GoodjobPanel.SetActive(true);

        SoundManager.instance.PlaySound("Cheerful Ambient", SoundType.SFX_1);

        //UI_ReferenceManager.instance.StartPanel.SetActive(true);
        //UI_ReferenceManager.instance.SelectTrainingPanel.SetActive(false);
        //UI_ReferenceManager.instance.tutorialScript._isOnTutorial = false;
        //UI_ReferenceManager.instance.tutorialScript._StepReset();

    }

}
