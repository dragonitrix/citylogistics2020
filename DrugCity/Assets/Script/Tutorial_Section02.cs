﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial_Section02 : MonoBehaviour {

	public bool _Section02;
    int step;
    public int _Step
    {
        get { return step; }
    }
	public List<GameObject> _Node = new List<GameObject>();

	public List<GameObject> _Tu;

	public List<GameObject> _HideTu;

	public GameObject[] _WarnningPopUp;

	public List<GameObject> _HideArrow;
	// Use this for initialization
	public void _isTutorial()
	{
		PlayerScript.Instance._ActionBtm [9].interactable = false;
		_Section02 = true;
		GetComponent<CardManager>()._StartGameSetup();
	}

	public void _CloseTapOnlyForThis(){
		if (_Section02 && _Step == 12) {
			_Tu [9].SetActive (true);
			_StepUp();
		}
	}

    public void _StepUp()
    {
        Debug.Log("Step: " + _Step);
        step++;
    }
	
	// Update is called once per fra
}
