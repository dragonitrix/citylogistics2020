﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;


public static class DataManager
{
    //public static static DataManager instance;

    //private void Awake()
    //{
    //    if (instance == null)
    //        instance = this;
    //    else
    //        Destroy(this);
    //    DontDestroyOnLoad(instance);
    //}

    //A method used to check if the data path is available
    public static bool isFilePathExist(string path)
    {
        return Directory.Exists(path);
    }

    public static void saveData<Type>(string dataPath, string fileName, Type SaveData)
    {
        //Check if the data path requested is available
        if (!isFilePathExist(dataPath))
        {
            //if not, created them
            Directory.CreateDirectory(dataPath);
        }

        //Convert to json text
        string json = JsonUtility.ToJson(SaveData, true);
        File.WriteAllText(dataPath + "/" + fileName + ".json", json);
    }




    public static void loadData<Type>(string fileName, ref Type dataContainer)
    {

        try
        {
            // Debug.Log("load na ja ");
            dataContainer = JsonUtility.FromJson<Type>(Resources.Load(fileName).ToString());
        }
        catch (System.Exception)
        {
            Debug.Log("There's no file in the path you request");
            return;
        }
    }

    public static bool checkData<Type>(string dataPath, string fileName)
    {
        if (isFilePathExist(dataPath))
            try
            {
                Type data = JsonUtility.FromJson<Type>(File.ReadAllText(dataPath + "/" + fileName + ".json"));
                //case this doesn't return null ref mean there's file in the path
                return true;
            }
            catch (System.Exception)
            {
                Debug.Log("There's no file in the path you request");
                return false;
            }
        else
            return false;
    }

}
