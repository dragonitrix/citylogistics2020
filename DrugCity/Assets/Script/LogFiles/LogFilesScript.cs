﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

//using GameAnalyticsSDK;
public class LogFilesScript : MonoBehaviour
{

    // Use this for initialization

    void Start()
    {
        //GameAnalytics.Initialize();
        //Debug.Log("Start GA");
    }

    public void _ResetLog()
    {
        PlayerPrefs.DeleteAll();
    }

    // Update is called once per frame

    public bool _isStartCounting;
    float _PlayTimeCounting;
    void FixedUpdate()
    {
        if (_isStartCounting)
        {
            _PlayTimeCounting += Time.deltaTime;
        }

        if (_isStartCountPerTurn)
        {
            _TimePerTurn += Time.deltaTime;
        }
    }

    public bool _isFirstTurn;
    public void _StartCountMethod()
    {
        _isStartCountPerTurn = true;
    }

    public void _FirstTurnOnly()
    {
        if (!_isFirstTurn)
        {
            _isStartCountPerTurn = true;
            _isFirstTurn = true;
        }
    }

    public List<int> _CheckActionById;

    public int _NumberOfMajorGoal; //Check
    public int _NumberOfSubGoal; // Check
    public int _GameTurn; // Check
    public int _UseAbilty;
    //public int _PlayTime; //Check

    public float _Minutes;
    public float _Seconds;

    public int _ShareKnowLedge;
    public string _EndGameBy; // Check
    public string _GameResult;

    public List<float> _TimePerMove_P1 = new List<float>();
    public List<float> _TimePerMove_P2 = new List<float>();

    public float _TimePerTurn;
    public bool _isStartCountPerTurn;

    public void _AddTime()
    {
        //Debug.Log("trun progress" + PlayerScript.Instance._State);

        switch (PlayerScript.Instance._State)
        {
            case 1:
                _TimePerMove_P1.Add(_TimePerTurn);
                break;
            case 0:
                _TimePerMove_P2.Add(_TimePerTurn);
                break;
        }
        //_TimePerMove_P1.Add(_TimePerTurn);
        _TimePerTurn = 0;
        _isStartCountPerTurn = false;
    }

    public void _CalBeforeWrtie()
    {
        if (GetComponent<PlayerScript>()._isCureType[1])
        {
            _NumberOfSubGoal++;
        }

        if (GetComponent<PlayerScript>()._isCureType[0])
        {
            _NumberOfSubGoal++;
        }

        if (_NumberOfSubGoal == 2)
        {
            _NumberOfMajorGoal = 01; // Yes
        }
        else
        {
            _NumberOfMajorGoal = 00; // No
        }

        switch (_GameResult)
        {
            case "Win":
                _GameResult = 01.ToString("00");
                break;
            case "Lose":
                _GameResult = 02.ToString("00");
                break;
            case "Restart":
                _GameResult = 03.ToString("00");
                break;

        }

        _Minutes = Mathf.Floor(_PlayTimeCounting / 60);
        _Seconds = Mathf.RoundToInt(_PlayTimeCounting % 60);

        if (!GetComponent<TutorialScript>()._isOnTutorial && !GetComponent<Tutorial_Section02>()._Section02)
        {
            _GameEndTime = System.DateTime.Now.ToString("yyMMdd") + "_" + System.DateTime.Now.ToString("hhmmss");
            WriteToLogFile();
        }
    }

    public void _AddActionById(int actionIndex)
    {
        if (actionIndex == 5)
        {
            Debug.Log("abilityUse");
        }
        this._CheckActionById.Add(actionIndex);
    }

    public void _IncresseTurn()
    {
        if (!GetComponent<TutorialScript>()._isOnTutorial && !GetComponent<Tutorial_Section02>()._Section02)
        {
            _GameTurn++;

            for (int i = 0; i < _CheckActionById.Count; i++)
            {
                switch (_CheckActionById[i])
                {
                    case 4:
                        _ShareKnowLedge++;
                        break;
                    case 5:
                        _UseAbilty++;
                        break;
                }
            }

            //foreach (int i in _CheckActionById)
            //{
            //    if (i == 5)
            //    {
            //        _UseAbilty++;
            //    }
            //
            //    if (i == 4)
            //    {
            //        _ShareKnowLedge++;
            //    }
            //}

            _CheckActionById.Clear();
        }

        Debug.Log("ability use: " + _UseAbilty);

    }

    public void _MedicSkill()
    {
        _AddActionById(5);
    }

    string _mode;
    bool _OnWriteLog;
    string _GameStartTime;
    string _GameEndTime;
    string _ModeName = "";

    public void _GetMode(string i)
    {
        _mode = i;
        //var _ModeName = "";
        _ModeName = "";
        switch (LanguageSettings.instance.gameMode)
        {
            case LanguageSettings.GameMode.Drug_City:
                _ModeName += "_drug";
                break;
            case LanguageSettings.GameMode.Green_City:
                _ModeName += "_green";
                break;
        }

        //switch (LanguageSettings.instance.language)
        //{
        //    case LanguageSettings.Language.Thai:
        //        modeName += "TH_";
        //        break;
        //    case LanguageSettings.Language.English:
        //        modeName += "EN_";
        //        break;
        //}

        _GameStartTime = System.DateTime.Now.ToString("yyMMdd") + "_" + System.DateTime.Now.ToString("hhmmss");

        //_GameStartTime = LanguageSettings.instance.startTime;

        _OnWriteLog = true;
    }


    public int _Experiment_Number;
    public void WriteToLogFile()
    {

        //string LogName = "GameLog - "+ _mode+ " - "+ _GameResult +" ["+ System.DateTime.Now.Day+"-"+System.DateTime.Now.Month+"-"+System.DateTime.Now.Year+"] - ["+System.DateTime.Now.ToString("hh-mm") +"]" ; // Change to data & Time

        string LogName = LanguageSettings.instance.startTime + _ModeName;
        //Debug.Log(LogName);
        //string Path = Application.dataPath +"/"+LogName+".txt";
        string Path = Application.dataPath + "/" + LogName + ".csv";
        //Debug.Log("CHECK");

        string old_content = "";

        if (!File.Exists(Path))
        {
            string headline = "";
            string[] cellHeadLine = {
            "Player1 id",
            "Player2 id",
            "Group id",
            "Experiment number",
            "Game mode",
            "Roles Player1",
            "Roles Player2",
            "Game start time",
            "Game end time",
            //"Order of game turn",
            "Player 1 time",
            "Player 2 time",
            "End by",
            "Number of game total turn ",
            "Number of reaching major goal",
            "Number of reaching sub goal",
            "Number of using special ability",
            "Number of Share Knowledge action"
        };
            for (int i = 0; i < cellHeadLine.Length; i++)
            {
                headline += cellHeadLine[i] + ",";
            }
            File.WriteAllText(Path, headline);
            old_content += headline;
        }
        else
        {
            old_content = File.ReadAllText(Path);
        }

        _Experiment_Number = PlayerPrefs.GetInt("_Experiment_Number", 0);

        int _Count = 0;
        string p1_timeString = "";
        string p2_timeString = "";
        float minutes = 0;
        float seconds = 0;

        for (int i = 0; i < _TimePerMove_P1.Count; i++)
        {
            _Count++;
            minutes = Mathf.Floor(_TimePerMove_P1[i] / 60);
            seconds = Mathf.RoundToInt(_TimePerMove_P1[i] % 60);
            p1_timeString += _Count.ToString("00") + "^" + minutes.ToString("00") + ":" + seconds.ToString("00"); // 01^1:33
            p1_timeString += "^";
        }

        _Count = 0;
        minutes = 0;
        seconds = 0;

        for (int i = 0; i < _TimePerMove_P2.Count; i++)
        {
            _Count++;
            minutes = Mathf.Floor(_TimePerMove_P2[i] / 60);
            seconds = Mathf.RoundToInt(_TimePerMove_P2[i] % 60);
            p2_timeString += _Count.ToString("00") + "^" + minutes.ToString("00") + ":" + seconds.ToString("00"); // 01^1:33
            p2_timeString += "^";
        }

        /*
        for (int i = 0; i < _GameTurn; i++)
        {
            _Count++;
            if (i == 0)
            {
                minutes = Mathf.Floor(_TimePerMove_P1[i] / 60);
                seconds = Mathf.RoundToInt(_TimePerMove_P1[i] % 60);
                p1_timeString += _Count.ToString("00") + "^" + minutes.ToString("00") + ":" + seconds.ToString("00"); // 01^1:33

                if ((i + 1) == _TimePerMove_P1.Count)
                {
                    p1_timeString += "^" + "00:00;"; //^02:30
                    break;
                }
                minutes = Mathf.Floor(_TimePerMove_P1[i + 1] / 60);
                seconds = Mathf.RoundToInt(_TimePerMove_P1[i + 1] % 60);
                p1_timeString += "^" + minutes.ToString("00") + ":" + seconds.ToString("00") + ";"; //^02:30
            }
            else
            {

                if (i + _Count > _GameTurn)
                {
                    break;
                }

                minutes = Mathf.Floor(_TimePerMove_P1[i + _Count - 1] / 60);
                seconds = Mathf.RoundToInt(_TimePerMove_P1[i + _Count - 1] % 60);
                p1_timeString += _Count.ToString("00") + "^" + minutes.ToString("00") + ":" + seconds.ToString("00"); // 01^1:33


                Debug.Log(i + _Count);
                if ((i + _Count) == _TimePerMove_P1.Count)
                {
                    p1_timeString += "^" + "00:00;"; //^02:30
                    break;
                }
                minutes = Mathf.Floor(_TimePerMove_P1[i + _Count] / 60);
                seconds = Mathf.RoundToInt(_TimePerMove_P1[i + _Count] % 60);
                p1_timeString += "^" + minutes.ToString("00") + ":" + seconds.ToString("00") + ";"; //^02:30


            }
        }
*/
        /*
                for (int i = 0; i < _GameTurn; i++)
                {
                    _Count++;
                    if (i == 0)
                    {
                        //    minutes = Mathf.Floor(_TimePerMove_P2[i] / 60);
                        //    seconds = Mathf.RoundToInt(_TimePerMove_P2[i] % 60);
                        //    p2_timeString += _Count.ToString("00") + "^" + minutes.ToString("00") + ":" + seconds.ToString("00"); // 01^1:33
                        //
                        //    if ((i + 1) == _TimePerMove_P2.Count)
                        //    {
                        //        p2_timeString += "^" + "00:00;"; //^02:30
                        //        break;
                        //    }
                        //    minutes = Mathf.Floor(_TimePerMove_P2[i + 1] / 60);
                        //    seconds = Mathf.RoundToInt(_TimePerMove_P2[i + 1] % 60);
                        //    p2_timeString += "^" + minutes.ToString("00") + ":" + seconds.ToString("00") + ";"; //^02:30
                    }
                    else
                    {
                        if (i + _Count > _GameTurn)
                        {
                            break;
                        }

                        minutes = Mathf.Floor(_TimePerMove_P2[i + _Count - 1] / 60);
                        seconds = Mathf.RoundToInt(_TimePerMove_P2[i + _Count - 1] % 60);
                        p2_timeString += _Count.ToString("00") + "^" + minutes.ToString("00") + ":" + seconds.ToString("00"); // 01^1:33


                        Debug.Log(i + _Count);
                        if ((i + _Count) == _TimePerMove_P2.Count)
                        {
                            p2_timeString += "^" + "00:00;"; //^02:30
                            break;
                        }
                        minutes = Mathf.Floor(_TimePerMove_P2[i + _Count] / 60);
                        seconds = Mathf.RoundToInt(_TimePerMove_P2[i + _Count] % 60);
                        p2_timeString += "^" + minutes.ToString("00") + ":" + seconds.ToString("00") + ";"; //^02:30
                    }
                }
        */
        //Debug.Log(p2_timeString);

        //string Content = "<head>" + _Experiment_Number.ToString ("000") + ";" + _mode + ";" + _GameStartTime + "<playgame>" + p1_timeString + "\n"
        //	+ "<head>" + _Experiment_Number.ToString ("000") + ";" + _mode + ";" + _GameStartTime + ";" + _GameEndTime 
        //    + "<endgame>" + _GameResult + ";" + _GameTurn + ";" + _NumberOfMajorGoal.ToString ("00") + ";" + _NumberOfSubGoal + ";" + _UseAbilty + ";" + _ShareKnowLedge;

        //string Content =
        //	"Experiment number,Game mode,Game start time,Order of game turn,Player 1 time,Player 2 time,Experiment number,Game mode,Game start time,Game end time,End by,Number of game total turn,Number of reaching major goal,Number of reaching sub goal,Number of using special ability,Number of Share Knowledge action" +
        //	"\n" +
        //	_Experiment_Number.ToString("000") + "," +
        //	_mode + "," +
        //	_GameStartTime + "," +
        //	_Experiment_Number.ToString("000") + "," +
        //	_mode + "," +
        //	_GameStartTime + "," +
        //	_GameEndTime + "," +
        //	_GameResult + "," +
        //	_GameTurn + "," +
        //	_NumberOfMajorGoal.ToString("00") + "," +
        //	_NumberOfSubGoal + "," +
        //	_UseAbilty + "," +
        //	_ShareKnowLedge + ","
        //;



        string Content = "";
        /*
        string[] cellHeadLine = {
            "Player1 id",
            "Player2 id",
            "Group id",
            "Experiment number",
            "Game mode",
            "Roles Player1",
            "Roles Player2",
            "Game start time",
            "Game end time",
            //"Order of game turn",
            "Player 1 time",
            "Player 2 time",
            "End by",
            "Number of game total turn ",
            "Number of reaching major goal",
            "Number of reaching sub goal",
            "Number of using special ability",
            "Number of Share Knowledge action"
        };

        for (int i = 0; i < cellHeadLine.Length; i++)
        {
            Content += cellHeadLine[i] + ",";
        }
        */
        Content += "\n";


        string[] cellContent = {
            LanguageSettings.instance.Player2ID.ToString("000") ,// "Player1 id",
            LanguageSettings.instance.Player1ID.ToString("000") ,// "Player2 id",
            LanguageSettings.instance.GroupID.ToString("000") ,// "Group id",
            _Experiment_Number.ToString("000"),// "Experiment number",
            _mode,// "Game mode",
            (PlayerScript.Instance._PlayerOneClass + 1).ToString("00"),// "Roles Player1",
            (PlayerScript.Instance._PlayerTwoClass + 1).ToString("00"),// "Roles Player2",
            _GameStartTime,// "Game start time",
            _GameEndTime,// "Game end time",
            // "Order of game turn",
            p1_timeString,// "Player 1 time",
            p2_timeString,// "Player 2 time",
            _GameResult,// "End by",
            _GameTurn.ToString(),// "Number of game total turn ",
            _NumberOfMajorGoal.ToString("00"),// "Number of reaching major goal",
            _NumberOfSubGoal.ToString("00"),// "Number of reaching sub goal",
            _UseAbilty.ToString("00"),// "Number of using special ability",
            _ShareKnowLedge.ToString("00")// "Number of Share Knowledge action"
        };

        //Debug.Log((PlayerScript.Instance._PlayerOneClass + 1).ToString("00") + "\n" +(PlayerScript.Instance._PlayerTwoClass + 1).ToString("00"));

        for (int i = 0; i < cellContent.Length; i++)
        {
            Content += cellContent[i] + ",";
        }
        //Content += "\n";

        //GameAnalytics.NewBusinessEvent(string currency, int amount, string itemType, string itemId, string cartType, string receipt, bool autoFetchReceipt);
        //GameAnalytics.NewResourceEvent (GA_Resource.GAResourceFlowType flowType, string resourceType, float amount, string itemType, string itemId);
        //GameAnalytics.NewProgressionEvent (GA_Progression.GAProgressionStatus progressionStatus, string progression01, string progression02);
        //GameAnalytics.NewDesignEvent (string eventName, float eventValue);
        //GameAnalytics.NewDesignEvent("_Experiment_Number", _Experiment_Number);
        //GA_Progression.GAProgressionStatus.
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "Experiment_Number" + _Experiment_Number.ToString("000"), _mode, _GameStartTime);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, "Experiment_Number" + _Experiment_Number.ToString("000"), _mode, _GameEndTime);
        //GameAnalytics.NewDesignEvent("_GameTurn", _GameTurn);
        //GameAnalytics.NewDesignEvent("_NumberOfMajorGoal", _NumberOfMajorGoal);
        //GameAnalytics.NewDesignEvent("_NumberOfSubGoal", _NumberOfSubGoal);
        //GameAnalytics.NewDesignEvent("_UseAbilty", _UseAbilty);
        //GameAnalytics.NewDesignEvent("_ShareKnowLedge", _ShareKnowLedge);


        //File.AppendAllText(Path, Content);

        Content = old_content + Content;
        File.WriteAllText(Path, Content);

        PlayerPrefs.SetInt("_Experiment_Number", _Experiment_Number + 1);
    }

    void OnApplicationQuit()
    {
        if (_OnWriteLog)
        {
            GetComponent<LogFilesScript>()._GameResult = "Restart";
            GetComponent<LogFilesScript>()._EndGameBy = "Restart";
            GetComponent<LogFilesScript>()._isStartCounting = false;
            GetComponent<LogFilesScript>()._CalBeforeWrtie();
        }
    }
}
