﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleStoryBGChange : MonoBehaviour
{

    public Sprite bg_thai;
    public Sprite bg_eng;


    // Start is called before the first frame update
    void Start()
    {

        switch (LanguageSettings.instance.language)
        {
            case LanguageSettings.Language.Thai:
                GetComponent<Image>().sprite = bg_thai;
                break;
            case LanguageSettings.Language.English:
                GetComponent<Image>().sprite = bg_eng;
                break;
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
