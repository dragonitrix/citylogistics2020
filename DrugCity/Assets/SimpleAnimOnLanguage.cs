﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAnimOnLanguage : MonoBehaviour
{
    public RuntimeAnimatorController drug;
    public RuntimeAnimatorController green;
    // Start is called before the first frame update
    void Start()
    {
        var lanstr = TextManager.instance.getLangString();
        if (lanstr.Contains("DRG"))
        {
            GetComponent<Animator>().runtimeAnimatorController = drug;
        }
        else if (lanstr.Contains("GRN"))
        {
            GetComponent<Animator>().runtimeAnimatorController = green;
        }
    }
}
