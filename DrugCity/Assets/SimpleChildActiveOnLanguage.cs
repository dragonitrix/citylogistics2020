﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleChildActiveOnLanguage : MonoBehaviour
{
    public enum Type
    {
        ON_MODE, ON_LANGUAGE
    }
    public Type type = Type.ON_MODE;
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        var lanstr = TextManager.instance.getLangString();

        switch (type)
        {
            case Type.ON_MODE:
                if (lanstr.Contains("DRG"))
                {
                    transform.GetChild(0).gameObject.SetActive(true);
                }
                else if (lanstr.Contains("GRN"))
                {
                    transform.GetChild(1).gameObject.SetActive(true);
                }
                break;
            case Type.ON_LANGUAGE:
                if (lanstr.Contains("EN"))
                {
                    transform.GetChild(0).gameObject.SetActive(true);
                }
                else if (lanstr.Contains("TH"))
                {
                    transform.GetChild(1).gameObject.SetActive(true);
                }
                break;
        }

    }
}
