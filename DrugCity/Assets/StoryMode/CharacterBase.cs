﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterBase : MonoBehaviour
{

    public Sprite[] _Image;

    public Image _TopPart;

    public void _ChangeImage(int i)
    {
        _TopPart.sprite = _Image[i];
    }
}
