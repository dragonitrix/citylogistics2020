﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;

public class StoryMode : MonoBehaviour
{


    public TextMeshProUGUI _TextBar;
    public Text[] _NameBar;

    public GameObject _LeftNamePanel;
    public GameObject _RightNamePanel;

    public GameObject[] _CharRight;

    public GameObject[] _CharLeft;

    public GameObject[] _ExtraPanel;
    public GameObject[] _ExtraPanel_02;

    public enum _Side
    {
        LEFT,
        RIGHT
    }

    public enum _Character
    {
        A,
        B,
        C
    }



    [System.Serializable]
    public class _StoryPage
    {
        public Sprite _BG;
        public _Character _Character;
        public _Side _CharacterSide;
        //public List<string> _Text;

        public List<int> _TextId;
        [HideInInspector]
        public Dictionary<string, string> _TextData = new Dictionary<string, string>();

        public List<string> _Text;

        public float delay = 0;

        public void ChangeLanguage(string lang)
        {
            var result = FindLanguage(lang);
            if (result != null)
            {
                _Text = result;
            }
            else
            {
                Debug.Log("text not found " + _TextId[0]);
            }
        }

        List<string> FindLanguage(string lang)
        {
            List<string> temp = new List<string>();

            // Debug.Log("text datacount : " + _TextData.Count);

            foreach (var item in _TextData)
            {
                //Debug.Log(item);
                if (item.Key == lang)
                {
                    //Debug.Log(item.Value);
                    temp.Add(item.Value);
                }
            }

            if (temp.Count == 0)
            {
                return null;
            }
            else
            {
                return temp;
            }



        }

    }

    [Header("SETUP")]

    public List<_StoryPage> _StoryStore;

    public int _CountBar;
    public int _InsideBarCount;

    private void Start()
    {
        //Debug.Log(_StoryStore[0]._Text.Count);
        _IntiBar(0);
        //Debug.Log(_StoryStore[_CountBar]._Text.Count);
        //Debug.Log(_InsideBarCount);



    }



    public void _IntiBar(int i)
    {

        foreach (GameObject x in _CharLeft)
        {
            x.SetActive(false);
        }

        foreach (GameObject x in _CharRight)
        {
            x.SetActive(false);
        }


        if (_StoryStore[i]._CharacterSide == _Side.LEFT)
        {
            _LeftNamePanel.SetActive(true);
            _RightNamePanel.SetActive(false);


            _CharLeft[(byte)_StoryStore[i]._Character].SetActive(true);
        }
        else // Right
        {
            _RightNamePanel.SetActive(true);
            _LeftNamePanel.SetActive(false);

            _CharRight[(byte)_StoryStore[i]._Character].SetActive(true);
        }

        _NameBar[(byte)_StoryStore[i]._CharacterSide].text = _StoryStore[i]._Character.ToString();


        _TextBar.text = _StoryStore[i]._Text[_InsideBarCount];
        Debug.Log("show text id : " + _StoryStore[i]._TextId[0]);

        var key = _StoryStore[i]._TextId[0] + ",";
        TextManager.instance.TUTORIAL_TEXT_DATA.Replace(key, key + "\n" + TextManager.instance.TUTORIAL_TEXT_INDEX + ",\n");




        foreach (Transform transform in _TextBar.transform)
        {
            Destroy(transform.gameObject);
        }
        /*
        if (_TextBar.text.Contains("${"))
        {

            var index = _TextBar.text.IndexOf("${");
            var iconindex = System.Int32.Parse(_TextBar.text[index + 2].ToString());
            var pos = _TextBar.textInfo.characterInfo[index].topLeft ;
            var icon = Instantiate(TextManager.instance.icons[iconindex], pos, Quaternion.identity);
            icon.transform.SetParent(_TextBar.transform,false);
            icon.transform.Translate(50,0,0);

           //_TextBar.text.Remove(index,4);
           //_TextBar.text.Insert(index,"    ");
            
        }
        */

        //_TextBar.GetComponent<TMPro.TextMeshProUGUI>().text = _StoryStore[i]._Text[_InsideBarCount];

        foreach (GameObject x in _ExtraPanel)
        {
            x.SetActive(false);
        }

        _ExtraPanel[i].SetActive(true);

        foreach (GameObject y in _ExtraPanel_02)
        {
            if (y != null)
            {
                y.SetActive(false);
            }

        }

        if (_ExtraPanel_02[i] != null)
        {
            _ExtraPanel_02[i].SetActive(true);
        }


    }

    bool isDelayed = false;

    IEnumerator next()
    {
        isDelayed = true;
        yield return new WaitForSeconds(_StoryStore[_CountBar].delay);

        isDelayed = false;

        if (_InsideBarCount >= _StoryStore[_CountBar]._Text.Count - 1)
        {
            _InsideBarCount = 0;
            _CountBar++;

            if (_CountBar >= _StoryStore.Count)
            {
                Debug.Log("Close");
                _CountBar = 0;
                _IntiBar(_CountBar);

                //File.WriteAllText(Application.dataPath + "/Resources/" + "stringStpryText_new" + ".json", TextManager.instance.TUTORIAL_TEXT_DATA);
                this.gameObject.SetActive(false);

                // Application.LoadLevel(0);
            }
            else
            {
                _IntiBar(_CountBar);
            }
        }
        else
        {

            _InsideBarCount++;
            _IntiBar(_CountBar);

        }

        if (_CountBar != 0)
        {
            _BackBTM.SetActive(true);
        }
        else
        {
            _BackBTM.SetActive(false);
        }

    }

    public void _Next()
    {
        if (isDelayed) return;
        StartCoroutine(next());
    }

    public GameObject _BackBTM;
    public void _Back()
    {
        _CountBar--;

        if (_CountBar <= 0)
        {
            _CountBar = 0;
        }

        if (_CountBar != 0)
        {
            _BackBTM.SetActive(true);
        }
        else
        {
            _BackBTM.SetActive(false);
        }



        _IntiBar(_CountBar);
        /*if (_InsideBarCount >= _StoryStore[_CountBar]._Text.Count - 1)
        {
            _InsideBarCount = 0;
            _CountBar++;

            if (_CountBar >= _StoryStore.Count)
            {
                Debug.Log("Close");
                this.gameObject.SetActive(false);
                Application.LoadLevel(0);
            }
            else
            {
                _IntiBar(_CountBar);
            }
        }
        else
        {

            _InsideBarCount++;
            _IntiBar(_CountBar);

        }*/

    }

}
