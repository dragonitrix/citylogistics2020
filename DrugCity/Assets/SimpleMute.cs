﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMute : MonoBehaviour
{
    [Header("audio source")]
    public AudioSource audioSource;
    
    public void mute()
    {
        audioSource.mute = !audioSource.mute;
    }

}
