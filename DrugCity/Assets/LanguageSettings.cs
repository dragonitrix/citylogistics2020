﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[ExecuteAlways]
public class LanguageSettings : MonoBehaviour
{
    public enum GameMode
    {
        Drug_City,
        Green_City
    }
    public GameMode gameMode;

    public string startTime;

    public enum Language
    {
        Thai,
        English
    }
    public Language language;

    public int GroupID;

    public int Player1ID;

    public int Player2ID;

    public static LanguageSettings instance;

    //public void Ping()
    //{
    //    //Debug.Log("ping");
    //    //GameObject.FindObjectOfType<simpleSelectLan>().skip();
    //}

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
        }

        this.startTime = System.DateTime.Now.ToString("yyMMdd") + "_" + System.DateTime.Now.ToString("hhmmss");

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            FindObjectOfType<CardManager>()._EndGame(true, "By Cured");
        }

        if (Input.GetKeyDown(KeyCode.L))
        {
			FindObjectOfType<CardManager> ()._EndGame (false, "By Out of Cubes");
        }
    }
}
