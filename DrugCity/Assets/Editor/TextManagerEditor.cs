﻿using UnityEngine;
using UnityEditor;
using System.IO;

[CustomEditor(typeof(LanguageSettings))]
public class TextManagerEditor : Editor
{
    //ChangeLanguage
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var mode = "";
        var lanq = "";

        switch (LanguageSettings.instance.language)
        {
            case LanguageSettings.Language.Thai:
                lanq = "TH";
                break;
            case LanguageSettings.Language.English:

                lanq = "EN";
                break;
        }
        switch (LanguageSettings.instance.gameMode)
        {
            case LanguageSettings.GameMode.Drug_City:
                mode = "DRG";
                break;
            case LanguageSettings.GameMode.Green_City:
                mode = "GRN";
                break;
        }

        if (GUILayout.Button("ChangeText"))
        {
            TextManager.instance.ChangeLanguage();
        }
        if (GUILayout.Button("To Green"))
        {
            TextManager.instance.ChangeLanguage("GRN_" + lanq);
        }
        if (GUILayout.Button("To Drug"))
        {
            TextManager.instance.ChangeLanguage("DRG_" + lanq);
        }
        if (GUILayout.Button("To Thai"))
        {
            TextManager.instance.ChangeLanguage(mode + "_TH");
        }
        if (GUILayout.Button("To Eng"))
        {
            TextManager.instance.ChangeLanguage(mode + "_EN");
        }


        if (GUILayout.Button("SAVE INDEX"))
        {
            File.WriteAllText(Application.dataPath + "/Resources/" + "stringWithNodename_new" + ".json", TextManager.instance.TUTORIAL_TEXT_DATA);
        }


    }
}

[CustomEditor(typeof(TextReplacer))]
public class TextReplacerEdior : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        if (GUILayout.Button("TEST"))
        {
        }
    }
}