﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UI_ReferenceManager : MonoBehaviour
{

    public static UI_ReferenceManager instance;

    public GameObject Canvas;
    public GameObject CanvasStory;
    public GameObject StartPanel;
    public GameObject TutorialPanel;
    public GameObject SelectTrainingPanel;
    public GameObject GoodjobPanel;

    [HideInInspector]
    public TutorialScript tutorialScript;
    [HideInInspector]
    public PlayerScript playerScript;
    [HideInInspector]
    public CardManager cardManager;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        tutorialScript = Canvas.GetComponent<TutorialScript>();
        playerScript = Canvas.GetComponent<PlayerScript>();
        cardManager = Canvas.GetComponent<CardManager>();
    }


    public void reloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
