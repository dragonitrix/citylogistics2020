﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class simpleSelectLan : MonoBehaviour
{
    [Header("canvas obj")]
    public GameObject cv_selectMode;
    public GameObject cv_selectLang;
    public GameObject cv_enterID;

    [Header("input id obj")]
    public InputField field_groupId;
    public InputField field_p1Id;
    public InputField field_p2Id;


    //[Header("main game canvas")]
    //public GameObject mainGame;

    public void OnSetGameMode(int mode)
    {
        //switch (mode)
        //{
        //    case 0:
        //        LanguageSettings.instance.gameMode = LanguageSettings.GameMode.Green_City;
        //        break;
        //    case 1:
        //        LanguageSettings.instance.gameMode = LanguageSettings.GameMode.Drug_City;
        //        break;
        //}
        //cv_selectMode.SetActive(false);
        //cv_selectLang.SetActive(true);

        switch (mode)
        {
            case 0:
                LanguageSettings.instance.gameMode = LanguageSettings.GameMode.Green_City;
                break;
            case 1:
                LanguageSettings.instance.gameMode = LanguageSettings.GameMode.Drug_City;
                break;
        }
        cv_selectMode.SetActive(false);

        LanguageSettings.instance.language = LanguageSettings.Language.Thai;

        cv_selectLang.SetActive(true);
    }
    public void OnSetLanguage(int lan)
    {
        switch (lan)
        {
            case 0:
                LanguageSettings.instance.language = LanguageSettings.Language.Thai;
                break;
            case 1:
                LanguageSettings.instance.language = LanguageSettings.Language.English;
                break;
        }
        cv_selectLang.SetActive(false);
        cv_enterID.SetActive(true);

    }

    public void OnSetID()
    {

        int groupID, p1ID, p2ID;

        try
        {
            groupID = int.Parse(field_groupId.text);
        }
        catch (System.Exception)
        {
            Debug.Log("GID parse error return");
            return;
        }

        try
        {
            p1ID = int.Parse(field_p1Id.text);
        }
        catch (System.Exception)
        {
            Debug.Log("P1ID parse error return");
            return;
        }

        try
        {
            p2ID = int.Parse(field_p2Id.text);
        }
        catch (System.Exception)
        {
            Debug.Log("P2ID parse error return");
            return;
        }

        if (p1ID == p2ID)
        {
            Debug.Log("ID duplicate error return");
            return;
        }

        LanguageSettings.instance.GroupID = groupID;
        LanguageSettings.instance.Player1ID = p1ID;
        LanguageSettings.instance.Player2ID = p2ID;

        LanguageSettings.instance.startTime += "_" + groupID.ToString("000"); ;

        cv_enterID.SetActive(false);
        //mainGame.SetActive(true);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }



}
