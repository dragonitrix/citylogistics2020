﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DigitalRuby.Tween;

public class ToastMessageManager : MonoBehaviour
{

    public static ToastMessageManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    //public class ToastMessageText
    //{
    //    public string key;
    //    public Dictionary<string, string> textDatas = new Dictionary<string, string>();
    //
    //}

    public Dictionary<string, Dictionary<string, string>> toastMessageText = new Dictionary<string, Dictionary<string, string>>();

    public GameObject toastMessage;
    public CanvasGroup toastMessage_canvas;

    bool isShown = false;
    float timer = 0;

    // Start is called before the first frame update
    void Start()
    {
        if (toastMessage == null)
        {
            toastMessage = transform.GetChild(0).gameObject;
        }
        toastMessage.GetComponent<Button>().onClick.AddListener(hideMessage);

        toastMessage_canvas = toastMessage.GetComponent<CanvasGroup>();
        toastMessage_canvas.alpha = 0;
        toastMessage_canvas.interactable = false;
        isShown = false;

        initText();

        //showMessage("แก๊งเรามีนโยบายใช้รถโดยสารสาธารณะในการบริการส่งของ\n\nสินค้าจะถูกไปกับรถโดยสารสาธารณะ โดยลูกค้าจะต้องไปรับของตามสถานีต่างๆ\nเราวางแผนส่งของตามตารางเดินรถ ยาเสพติดก็จะถูกซ่อนอยู่กับของอื่นๆนั้นหล่ะ\n\nวิธีนี้เป็นการประหยัดค่าขนส่งด้วย แถมยังช่วยท่านหัวหน้า (ผู้ว่าฯ) ให้ได้หน้าเรื่องลดมลภาวะด้วย..ดีจริงๆ ", 3);

    }

    string getTextFromKey(string key)
    {
        var textData = toastMessageText[key];

        var lanstr = TextManager.instance.getLangString();
        var result = "";
        textData.TryGetValue(lanstr, out result);
        return result;
    }

    public void showMessage(string key)
    {
        showMessage(key, 10f);
    }


    public void showMessage(string key, float duration)
    {

        var text = getTextFromKey(key);
        if (text == "") return;

        System.Action<ITween<float>> onUpdate = (t) =>
        {
            toastMessage_canvas.alpha = t.CurrentValue;
        };
        System.Action<ITween<float>> onComplete = (t) =>
        {
        };
        // completion defaults to null if not passed in
        gameObject.Tween("showToastMessage", 0, 1, 0.5f, TweenScaleFunctions.CubicEaseOut, onUpdate, onComplete);
        toastMessage_canvas.interactable = true;
        toastMessage_canvas.blocksRaycasts = true;
        isShown = true;
        timer = duration;

        var tmp = toastMessage.GetComponentInChildren<TextMeshProUGUI>();
        tmp.text = text;
        tmp.ForceMeshUpdate();

        toastMessage.GetComponent<RectTransform>().sizeDelta = new Vector2(
            toastMessage.GetComponent<RectTransform>().sizeDelta.x,
            100
        );
        // Debug.Log(tmp.isTextOverflowing || tmp.isTextTruncated);
        do
        {
            toastMessage.GetComponent<RectTransform>().sizeDelta = new Vector2(
                toastMessage.GetComponent<RectTransform>().sizeDelta.x,
                toastMessage.GetComponent<RectTransform>().sizeDelta.y + 2
            );
            tmp.ForceMeshUpdate();
        } while (tmp.isTextOverflowing || tmp.isTextTruncated);
    }

    public void hideMessage()
    {
        //        Debug.Log("???");
        TweenFactory.RemoveTweenKey("hideToastMessage", TweenStopBehavior.DoNotModify);

        System.Action<ITween<float>> onUpdate = (t) =>
        {
            toastMessage_canvas.alpha = t.CurrentValue;
        };
        System.Action<ITween<float>> onComplete = (t) =>
        {
        };
        // completion defaults to null if not passed in
        gameObject.Tween("hideToastMessage", 1, 0, 0.5f, TweenScaleFunctions.CubicEaseOut, onUpdate, onComplete);
        toastMessage_canvas.interactable = false;
        toastMessage_canvas.blocksRaycasts = false;
        isShown = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (isShown)
        {
            if (timer > 0)
            {
                timer -= Time.deltaTime;
            }
            else
            {
                timer = 0;
                this.hideMessage();
            }
        }
    }

    void initText()
    {

        var textDatas = new Dictionary<string, string>();

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "แก๊งเรามีนโยบายใช้รถโดยสารสาธารณะในการบริการส่งของ\n\nสินค้าจะถูกไปกับรถโดยสารสาธารณะ โดยลูกค้าจะต้องไปรับของตามสถานีต่างๆ\nเราวางแผนส่งของตามตารางเดินรถ ยาเสพติดก็จะถูกซ่อนอยู่กับของอื่นๆนั้นหล่ะ\n\nวิธีนี้เป็นการประหยัดค่าขนส่งด้วย แถมยังช่วยท่านหัวหน้า (ผู้ว่าฯ) ให้ได้หน้าเรื่องลดมลภาวะด้วย..ดีจริงๆ ");
        textDatas.Add("DRG_EN", "Our gang has policy to deliver our drug by using public transportation\n\nOur customers can pick up our drugs along the tram line. The drugs are hidden in the normal package. \n\nThe policy is not only save transportation cost of our company but it also makes our boss looklike an environmentalist mayor.....so good. \n");
        textDatas.Add("GRN_TH", "บริษัทเรามีนโยบายใช้รถโดยสารสาธารณะในการบริการส่งของ\n\nสินค้าจะถูกไปกับรถโดยสารสาธารณะ โดยลูกค้าจะต้องไปรับของตามสถานีต่างๆ\nเราวางแผนส่งสินค้าตามตารางเดินรถ \n\nวิธีนี้เป็นการประหยัดค่าขนส่งด้วย แถมยังช่วยลดมลภาวะให้เมืองของเราด้วย..ดีจริงๆ ");
        textDatas.Add("GRN_EN", "Our gang has policy to deliver our products by using public transportation\n\nOur customers can pick up our products along the tram line. The products are hidden in the normal package. \n\nThe policy save transportation cost of our company. ");
        toastMessageText.Add("move_tram", textDatas);

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "แก๊งเราส่งยาฯให้ลูกค้าที่อยู่ในแหล่งชุมชนที่ถนนแคบ หรือตามซอกซอยโดยใช้รถจักรยาน\n\nบริการนี้นอกจากเข้าถึงลูกค้าที่บ้านอยู่ในซอกซอยได้มากขึ้น ยังช่วยลดมลพิษที่เกิดจากเครื่องยนต์ของรถอีกด้วย\n\nทีนี้แก๊งเราจะได้กระจายอำนาจและยาฯได้ทั่วถึงเลยทีเดียว..ฮี่ๆ\n\n");
        textDatas.Add("DRG_EN", "Our gange use BICYCLE to deliver drugs to the customers who live in the restic area and narrow street.\r\n\r\nThe service can increase accesbility and decrease air pullutions which casued by vehicles' engine.\r\n\r\n\r\n");
        textDatas.Add("GRN_TH", "บริษัทเราส่งสินค้าให้ลูกค้าที่อยู่ในแหล่งชุมชนที่ถนนแคบ หรือตามซอกซอย โดยใช้รถมอเตอร์ไซด์\r\n\r\nบริการนี้นอกจากเข้าถึงลูกค้าที่บ้านอยู่ในซอกซอยได้มากขึ้น ยังช่วยลดมลพิษที่เกิดจากเครื่องยนต์ของรถอีกด้วย\r\n\n");
        textDatas.Add("GRN_EN", "We use BICYCLE to deliver products to the customers who live in the restic area and narrow street.\r\n\r\nThe service can increase accesbility and decrease air pullutions which casued by vehicles' engine.\r\n\r\n\r\n");
        toastMessageText.Add("move_bicycle", textDatas);

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "แก๊งเราส่งยาฯให้ลูกค้าที่อยู่ในแหล่งชุมชนที่ถนนแคบ หรือตามซอกซอย โดยใช้รถมอเตอร์ไซด์\nคนขับพวกนี้ส่วนใหญ่ก็เป็นเด็กแว๊นในแก๊งเราที่ชำนาญเส้นทาง ทั้งนั้นหล่ะ\n\nบริการนี้นอกจากเข้าถึงลูกค้าที่บ้านอยู่ในซอกซอยได้มากขึ้น ยังรวดเร็ว ประหยัดเวลาและลดมลพิษที่เกิดจากรถใหญ่อีกด้วย\n\nส่งของแบบนี้ตำรวจยากที่จะไล่จับพวกเราทันเลยหล่ะ");
        textDatas.Add("DRG_EN", "Our gang use the bikers who are our staff to deliver drugs to the customers who live in the restriction areas. The bikers know the shortcuts and the street so well. \n\nWe can decrease delivery time, incease accesiblity and also avoid the police!! \n\n");
        textDatas.Add("GRN_TH", "บริษัทเราส่งสินค้าให้ลูกค้าที่อยู่ในแหล่งชุมชนที่ถนนแคบ หรือตามซอกซอย โดยใช้รถมอเตอร์ไซด์ คนขับส่วนใหญ่เป็นคนในพื้นที่ที่ชำนาญเส้นทาง\n\n\nบริการนี้นอกจากเข้าถึงลูกค้าที่บ้านอยู่ในซอกซอยได้มากขึ้น ยังรวดเร็ว ประหยัดเวลาและลดมลพิษที่เกิดจากรถใหญ่อีกด้วย");
        textDatas.Add("GRN_EN", "We use the driver who know the shortcuts and the street  to deliver products to the customers who live in the restriction areas.\n\nWe can decrease delivery time, incease accesiblity. \n\n");
        toastMessageText.Add("move_shareVehicle", textDatas);

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "เยี่ยม... ลดปัญหาการจราจรแอดอัดได้แล้ว\nพวกเราจะได้ส่งของกันคล่องๆหน่อย พวกตำรวจยิ่งจับตามองเราอยู่");
        textDatas.Add("GRN_TH", "เยี่ยม... ลดปัญหาการจราจรแอดอัดได้แล้ว\nพวกเราจะได้ส่งสินค้ากันได้อย่างคล่องตัวหน่อย");
        toastMessageText.Add("release", textDatas);

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "เยี่ยม... ลดปัญหาการจราจรแอดอัดได้แล้ว\nพวกเราจะได้ส่งของกันคล่องๆหน่อย พวกตำรวจยิ่งจับตามองเราอยู่");
        textDatas.Add("DRG_EN", "Cool..traffic congestion here is released. \nWe now have better transportation flow and out of police's sight.\n\n");
        textDatas.Add("GRN_TH", "เยี่ยม... ลดปัญหาการจราจรแอดอัดได้แล้ว\nพวกเราจะได้ส่งสินค้ากันได้อย่างคล่องตัวหน่อย");
        textDatas.Add("GRN_EN", "Cool..traffic congestion here is released. \nWe now have better transportation flow.");
        toastMessageText.Add("locate", textDatas);

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "ตอนนี้เราสร้าง UDC สำเร็จแล้ว !!\n\nเมื่อได้เอกสารยินยอมจากคนในชุมชนที่เราไปข่มขู่มาจนครบ 3 ใบ หัวหน้า (ผู้ว่าฯ) ก็สามารถเซนต์อนุมัติเงินงบประมาณให้บริษัทผู้ให้บริการลอจิสติกส์ (Logistics Service Provider: LSP) ที่เป็นคนของแก๊งเราให้สร้าง UDC ได้\n\nตอนนี้แก๊งเรามีคลังเก็บสินค้าขนาดและอู่เปลี่ยนรถเพื่อถ่ายของมาขนส่งรถคันเล็กอยู่ทั่วเมือง\n\nคราวนี้หล่ะะ เราจะได้กระจายยาเสพติดได้สะดวกทั่วถึง แถมหัวหน้าเรายังได้หน้าด้วยว่าพยายามรักษาสิ่งแวดล้อมโดยการลดการขนส่งโดยใช้รถใหญ่จากนอกเมือง");
        textDatas.Add("DRG_EN", "The UDC are constructed.. Hoorey!!\n\nAfter we get approval from the residents (3 street cards with  the same color) , the mayor then can now allocate budget to Logistics Service Provider/ LSP for onstructing UDCs. \n\nWe now have facilities (warehouse and garage) for supporting delivery of our products. \n\nOur city has an efficient distribution flow and decrease pollution which caused by vehicles.\n");
        textDatas.Add("GRN_TH", "ตอนนี้เราสร้าง UDC สำเร็จแล้ว !!\n\nเมื่อได้เอกสารยินยอมจากคนในชุมชนที่เราไปข่มขู่มาจนครบ 3 ใบ หัวหน้า (ผู้ว่าฯ) ก็สามารถเซนต์อนุมัติเงินงบประมาณให้บริษัทผู้ให้บริการลอจิสติกส์ (Logistics Service Provider: LSP) ที่เป็นคนของแก๊งเราให้สร้าง UDC ได้\n\nตอนนี้แก๊งเรามีคลังเก็บสินค้าขนาดและอู่เปลี่ยนรถเพื่อถ่ายของมาขนส่งรถคันเล็กอยู่ทั่วเมือง\n\nคราวนี้หล่ะะ เราจะได้กระจายยาเสพติดได้สะดวกทั่วถึง แถมหัวหน้าเรายังได้หน้าด้วยว่าพยายามรักษาสิ่งแวดล้อมโดยการลดการขนส่งโดยใช้รถใหญ่จากนอกเมือง");
        textDatas.Add("GRN_EN", "The UDC are constructed.. Hoorey!!\n\nAfter we get approval from the residents (3 street cards with  the same color) , the mayor then can now allocate budget to Logistics Service Provider/ LSP for onstructing UDCs. \n\nWe now have facilities (warehouse and garage) for supporting delivery of our products. \n\nOur city has an efficient distribution flow and decrease pollution which caused by vehicles.\n");
        toastMessageText.Add("construct", textDatas);

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "ประชุมร่วมมือระหว่าง 2 แก๊งเพื่อแลกเปลี่ยน solution ในการแก้ปัญหารถติด \n\nตอนนี้เราส่งยาฯลำบากเพราะการจาราจรติดขัด เราต้องเรียกหัวหน้าแก๊งที่ดูแลพื้นที่ฟ้าและแดงมาคุยกันเพื่อแลกเปลี่ยนประสบการณ์ในการแก้ปัญหารถติดอย่างยั้งยืน (Sustainable solution) \n");
        textDatas.Add("DRG_EN", "The meeting between 2 gansters  is needed to exchange solutions for traffic congestions problems. \n\nThe traffic congestions cased the problems to our gang. We need better transporation flow for distribuing drugs. The boss of 2 gansters: Blue and Red need to find sustainable solutions together.\n\n");
        textDatas.Add("GRN_TH", "ประชุมร่วมมือระหว่าง 2 บริษัทเพื่อแลกเปลี่ยน solution ในการแก้ปัญหารถติด \n\nตอนนี้เราส่งสินค้าลำบากเพราะการจาราจรติดขัด เราต้องหารือระหว่างบริษัทที่ทำธุรกิจในพื้นที่สีฟ้าและสีแดงที่เพื่อแลกเปลี่ยนประสบการณ์ในการแก้ปัญหารถติดอย่างยั้งยืน (Sustainable solution) \n");
        textDatas.Add("GRN_EN", "The meeting between 2 companies  is needed to exchange solutions for traffic congestions problems. \n\nThe traffic congestions cased the problems to our companies. We need better transporation flow for distrubiting our products. The boss of 2 companies: Blue and Red need to find sustainable solutions together.\n\n");
        toastMessageText.Add("shareSolution", textDatas);


        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "ผู้ค้าปลีก (Retailer)/ เอเย่นค้ายาฯ (Drug agent) เลือกใช้ยานพาหนะคันเล็กเพื่อส่งยาฯเป็นการช่วยลดจำนวนรถบนถนน ทำให้สามารถใช้ 1 action ในการลบ icon รถทั้งหมดได้้\n\n");
        textDatas.Add("GRN_TH", "ผู้ค้าปลีก (Retailer) เลือกใช้ยานพาหนะคันเล็กเพื่อส่งสินค้าเป็นการช่วยลดจำนวนรถบนถนน ทำให้สามารถใช้ 1 action ในการลบ icon รถทั้งหมดได้้\n");
        toastMessageText.Add("retailer_1", textDatas);


        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "-ผู้ค้าปลีก (Retailer) จัดตารางเวลาในการจอดรถเพื่อขนถ่ายสินค้า (unloading) ลงบริเวณหน้าร้านตามเวลาที่เทศบาลกำหนด ทำให้ icon รถจะไม่ถูกสุ่มบนถนนที่ผู้ค้าปลีกอยู่ หรือก็คือไม่เกิดปัญหาการจราจรติดขัดบนถนนเส้นที่ผู้ค้าปลีกอยู่ ");
        textDatas.Add("GRN_TH", "-ผู้ค้าปลีก (Retailer) จัดตารางเวลาในการจอดรถเพื่อขนถ่ายสินค้า (unloading) ลงบริเวณหน้าร้านตามเวลาที่เทศบาลกำหนด ทำให้ icon รถจะไม่ถูกสุ่มบนถนนที่ผู้ค้าปลีกอยู่ หรือก็คือไม่เกิดปัญหาการจราจรติดขัดบนถนนเส้นที่ผู้ค้าปลีกอยู่ ");
        toastMessageText.Add("retailer_2", textDatas);

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "นายกเทศมนตรีฯ (Mayor)/ หัวหน้าแก๊ง (Corrupted mayor) สามารถสั่งย้ายผู้เล่นอื่นเพื่อไปช่วยแก้ปัญหารถติดที่ถนนต่างๆใน map ได้ (สามารถใช้ action ในการย้ายตำแหน่งของผู้เล่นอื่นได้)\n\n");
        textDatas.Add("GRN_TH", "นายกเทศมนตรีฯ (Mayor) สามารถสั่งย้ายผู้เล่นอื่นเพื่อไปช่วยแก้ปัญหารถติดที่ถนนต่างๆใน map ได้ (สามารถใช้ action ในการย้ายตำแหน่งของผู้เล่นอื่นได้)\n");
        toastMessageText.Add("mayor_1", textDatas);

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "-นายกเทศมนตรีฯสามารถเรียกประชุมลูกน้องในแก๊งคือ ผู้ค้าปลีกและ ผู้ให้บริการลอจิสติกส์ มาประชุมในที่เดียวกันได้ (สามารถใช้ action ในการย้ายผู้เล่นไปยังตำแหน่งทีอีกผู้เล่นอยู่ได้)");
        textDatas.Add("GRN_TH", "-นายกเทศมนตรีฯสามารถเรียกประชุม ผู้ค้าปลีกและ ผู้ให้บริการลอจิสติกส์ มาประชุมในที่เดียวกันได้ (สามารถใช้ action ในการย้ายผู้เล่นไปยังตำแหน่งทีอีกผู้เล่นอยู่ได้)");
        toastMessageText.Add("mayor_2", textDatas);

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "เนื่องจากเป็นผู้ให้บริการลอจิสติกส์ (LSP)/ คนกระจายยา (Drug distributor) ผู้เล่นสามารถใช้ 1 action ในการ \"ระบุตำแหน่ง UDC ( Locate UDC landmark)\" โดยผู้เล่นไม่ต้องทิ้งการ์ดใดๆ \n\n");
        textDatas.Add("GRN_TH", "เนื่องจากเป็นผู้ให้บริการลอจิสติกส์ (LSP) ผู้เล่นสามารถใช้ 1 action ในการทำ Locate UDC landmark โดยผู้เล่นไม่ต้องทิ้งการ์ดใดๆ \n");
        toastMessageText.Add("lsp_1", textDatas);

        textDatas = new Dictionary<string, string>();
        textDatas.Add("DRG_TH", "เมื่อ LSP ทำการเปลี่ยนรถที่ UDC โดยถ่ายของ (unloading) ใส่ยานพาหนะขนาดเล็ก (จังหวะนี้เราจะซ่อนยาฯรวมไปกับของ) เพื่อเข้าไปส่งของให้บ้านในซอกซอยและถนนแคบๆ ทำให้เดินทางได้เร็วขึ้น ดังนั้นเมื่อ LSP อยู่ที่ UDC landmark (Pin) จะสามารถเดินทางไปยังถนนเส้นใดก็ได้ใน map  โดยใช้แค่ 1 action\n");
        textDatas.Add("GRN_TH", "เมื่อ LSP ทำการเปลี่ยนรถที่ UDC โดยถ่ายของ (unloading) ใส่ยานพาหนะขนาดเล็ก เพื่อเข้าไปส่งของให้บ้านในซอกซอยและถนนแคบๆ ทำให้เดินทางได้เร็วขึ้น ดังนั้นเมื่อ LSP อยู่ที่ UDC landmark (Pin) จะสามารถเดินทางไปยังถนนเส้นใดก็ได้ใน map  โดยใช้แค่ 1 action");
        toastMessageText.Add("lsp_2", textDatas);

    }

}
