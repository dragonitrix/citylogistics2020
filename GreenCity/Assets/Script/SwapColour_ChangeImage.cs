﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwapColour_ChangeImage : MonoBehaviour
{

    public int _Id;
    public Color32 i;

    public bool _isMaunalSetUp;
    public Sprite[] _IMG;

public bool _isStart;
    public void Start(){
        if(_isStart){
            _GetImage();
        }
    }
    // Start is called before the first frame update
    public void _GetImage()
    {
        i = GetComponent<Image>().color;

        if (_isMaunalSetUp)
        {
            if (i.r == 245) // Orange
            {

                //Debug.Log("CHECK A" + GetComponent<Image>().color.r;
                GetComponent<Image>().sprite = _IMG[0];
                GetComponent<Image>().color = Color.white;
            }
            else
            {
                //Debug.Log("CHECK B" + GetComponent<Image>().color.r + " : " + GetComponent<Image>().color.g + " : " + GetComponent<Image>().color.b);
                GetComponent<Image>().sprite = _IMG[1];
                GetComponent<Image>().color = Color.white;
            }
        }
        else
        {
            if (i.r == 245) // Orange
            {

                //Debug.Log("CHECK A" + GetComponent<Image>().color.r;
                GetComponent<Image>().sprite = Resources.Load<Sprite>("OrangeCard_Player");
                GetComponent<Image>().color = Color.white;
            }
            else
            {
                //Debug.Log("CHECK B" + GetComponent<Image>().color.r + " : " + GetComponent<Image>().color.g + " : " + GetComponent<Image>().color.b);
                GetComponent<Image>().sprite = Resources.Load<Sprite>("BlueCard_Player");
                GetComponent<Image>().color = Color.white;
            }
        }
        
    }

}
