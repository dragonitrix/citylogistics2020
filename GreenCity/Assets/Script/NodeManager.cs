﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour {

	public GameObject[] _Node;
	public GameObject _CurrectNode;

	public int _OutBreak;
	public GameObject _Manager;

	public void _Reset(){
		foreach (GameObject i in _Node) {
			i.GetComponent<NodeScript> ()._isFromOutBreak = false;
			i.GetComponent<NodeScript> ()._isOutBreak = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
