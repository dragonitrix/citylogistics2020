﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StoryMode : MonoBehaviour
{

   
    public Text _TextBar;
    public Text[] _NameBar;

    public GameObject _LeftNamePanel;
    public GameObject _RightNamePanel;

    public GameObject[] _CharRight;

    public GameObject[] _CharLeft;

    public GameObject[] _ExtraPanel;
    public GameObject[] _ExtraPanel_02;

    public enum _Side
    {
        LEFT,
        RIGHT
    }

    public enum _Character
    {
        A,
        B,
        C
    }

  

    [System.Serializable]
    public class _StoryPage
    {
        public Sprite _BG;
        public _Character _Character;
        public _Side _CharacterSide;
        public List<string> _Text;
    }

    [Header("SETUP")]

    public List<_StoryPage> _StoryStore;

    public int _CountBar;
    public int _InsideBarCount;

    private void Start()
    {
        _IntiBar(0);
        Debug.Log(_StoryStore[_CountBar]._Text.Count);
        Debug.Log(_InsideBarCount);
    }

    public void _IntiBar(int i)
    {

        foreach (GameObject x in _CharLeft)
        {
            x.SetActive(false);
        }

        foreach (GameObject x in _CharRight)
        {
            x.SetActive(false);
        }


        if (_StoryStore[i]._CharacterSide == _Side.LEFT)
        {
            _LeftNamePanel.SetActive(true);
            _RightNamePanel.SetActive(false);

            
            _CharLeft[(byte)_StoryStore[i]._Character].SetActive(true);
        }
        else // Right
        {
            _RightNamePanel.SetActive(true);
            _LeftNamePanel.SetActive(false);

            _CharRight[(byte)_StoryStore[i]._Character].SetActive(true);
        }

        _NameBar[(byte)_StoryStore[i]._CharacterSide].text = _StoryStore[i]._Character.ToString() ;


        _TextBar.text = _StoryStore[i]._Text[_InsideBarCount];

        foreach(GameObject x in _ExtraPanel)
        {
            x.SetActive(false);
        }

        _ExtraPanel[i].SetActive(true);

        foreach (GameObject y in _ExtraPanel_02)
        {
            if (y != null)
            {
                y.SetActive(false);
            }
           
        }

        if (_ExtraPanel_02[i] != null)
        {
            _ExtraPanel_02[i].SetActive(true);
        }

       
    }

    public void _Next()
    {
        
       
        if (_InsideBarCount >= _StoryStore[_CountBar]._Text.Count-1)
        {
            _InsideBarCount = 0;
            _CountBar++;

            if (_CountBar >= _StoryStore.Count)
            {
                Debug.Log("Close");
                this.gameObject.SetActive(false);
                Application.LoadLevel(0);
            }
            else
            {
                _IntiBar(_CountBar);
            }
        }
        else
        {
            
            _InsideBarCount++;
            _IntiBar(_CountBar);

        }

        if (_CountBar != 0)
        {
            _BackBTM.SetActive(true);
        }
        else
        {
            _BackBTM.SetActive(false);
        }

    }

    public GameObject _BackBTM;
    public void _Back()
    {

        

        _CountBar--;

        if (_CountBar <= 0)
        {
            _CountBar = 0;
        }

        if (_CountBar != 0)
        {
            _BackBTM.SetActive(true);
        }
        else
        {
            _BackBTM.SetActive(false);
        }

        

        _IntiBar(_CountBar);
        /*if (_InsideBarCount >= _StoryStore[_CountBar]._Text.Count - 1)
        {
            _InsideBarCount = 0;
            _CountBar++;

            if (_CountBar >= _StoryStore.Count)
            {
                Debug.Log("Close");
                this.gameObject.SetActive(false);
                Application.LoadLevel(0);
            }
            else
            {
                _IntiBar(_CountBar);
            }
        }
        else
        {

            _InsideBarCount++;
            _IntiBar(_CountBar);

        }*/

    }

}
